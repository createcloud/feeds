<?php
/**
 * @file
 * ccloud_feed_item_operation.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ccloud_feed_item_operation_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'node_operation';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Node operation';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '20, 50, 100';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'views_json';
  $handler->display->display_options['style_options']['root_object'] = '';
  $handler->display->display_options['style_options']['top_child_object'] = '';
  $handler->display->display_options['style_options']['plaintext_output'] = 1;
  $handler->display->display_options['style_options']['remove_newlines'] = 0;
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['style_options']['object_arrays'] = 0;
  $handler->display->display_options['style_options']['numeric_strings'] = 0;
  $handler->display->display_options['style_options']['bigint_string'] = 0;
  $handler->display->display_options['style_options']['pretty_print'] = 0;
  $handler->display->display_options['style_options']['unescaped_slashes'] = 0;
  $handler->display->display_options['style_options']['unescaped_unicode'] = 0;
  $handler->display->display_options['style_options']['char_encoding'] = array();
  /* Field: Content: Node ID */
  $handler->display->display_options['fields']['field_id_operated']['id'] = 'field_id_operated';
  $handler->display->display_options['fields']['field_id_operated']['table'] = 'field_data_field_id_operated';
  $handler->display->display_options['fields']['field_id_operated']['field'] = 'field_id_operated';
  $handler->display->display_options['fields']['field_id_operated']['label'] = 'nid';
  $handler->display->display_options['fields']['field_id_operated']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Operation */
  $handler->display->display_options['fields']['field_operation']['id'] = 'field_operation';
  $handler->display->display_options['fields']['field_operation']['table'] = 'field_data_field_operation';
  $handler->display->display_options['fields']['field_operation']['field'] = 'field_operation';
  $handler->display->display_options['fields']['field_operation']['label'] = 'operation';
  $handler->display->display_options['fields']['field_operation']['type'] = 'list_key';
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'YmdHis';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = 'time';
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_value'] = 'return $row->created;';
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
  print $value;
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Sort criterion: Content: Node ID (field_id_operated) */
  $handler->display->display_options['sorts']['field_id_operated_value']['id'] = 'field_id_operated_value';
  $handler->display->display_options['sorts']['field_id_operated_value']['table'] = 'field_data_field_id_operated';
  $handler->display->display_options['sorts']['field_id_operated_value']['field'] = 'field_id_operated_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ccloud_node_operation' => 'ccloud_node_operation',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Since id */
  $handler->display->display_options['filters']['field_id_operated_value']['id'] = 'field_id_operated_value';
  $handler->display->display_options['filters']['field_id_operated_value']['table'] = 'field_data_field_id_operated';
  $handler->display->display_options['filters']['field_id_operated_value']['field'] = 'field_id_operated_value';
  $handler->display->display_options['filters']['field_id_operated_value']['ui_name'] = 'Since id';
  $handler->display->display_options['filters']['field_id_operated_value']['operator'] = '>';
  $handler->display->display_options['filters']['field_id_operated_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_id_operated_value']['expose']['operator_id'] = 'field_id_operated_value_op';
  $handler->display->display_options['filters']['field_id_operated_value']['expose']['label'] = 'Since id';
  $handler->display->display_options['filters']['field_id_operated_value']['expose']['operator'] = 'field_id_operated_value_op';
  $handler->display->display_options['filters']['field_id_operated_value']['expose']['identifier'] = 'since_id';
  $handler->display->display_options['filters']['field_id_operated_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Max id */
  $handler->display->display_options['filters']['field_id_operated_value_1']['id'] = 'field_id_operated_value_1';
  $handler->display->display_options['filters']['field_id_operated_value_1']['table'] = 'field_data_field_id_operated';
  $handler->display->display_options['filters']['field_id_operated_value_1']['field'] = 'field_id_operated_value';
  $handler->display->display_options['filters']['field_id_operated_value_1']['ui_name'] = 'Max id';
  $handler->display->display_options['filters']['field_id_operated_value_1']['operator'] = '<';
  $handler->display->display_options['filters']['field_id_operated_value_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_id_operated_value_1']['expose']['operator_id'] = 'field_id_operated_value_1_op';
  $handler->display->display_options['filters']['field_id_operated_value_1']['expose']['label'] = 'Max id';
  $handler->display->display_options['filters']['field_id_operated_value_1']['expose']['operator'] = 'field_id_operated_value_1_op';
  $handler->display->display_options['filters']['field_id_operated_value_1']['expose']['identifier'] = 'max_id';
  $handler->display->display_options['filters']['field_id_operated_value_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'api/operation';
  $export['node_operation'] = $view;

  return $export;
}
