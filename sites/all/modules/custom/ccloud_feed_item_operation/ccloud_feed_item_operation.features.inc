<?php
/**
 * @file
 * ccloud_feed_item_operation.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ccloud_feed_item_operation_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ccloud_feed_item_operation_node_info() {
  $items = array(
    'ccloud_node_operation' => array(
      'name' => t('内容操作'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
