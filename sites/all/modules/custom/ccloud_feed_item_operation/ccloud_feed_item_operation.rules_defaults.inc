<?php
/**
 * @file
 * ccloud_feed_item_operation.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ccloud_feed_item_operation_default_rules_configuration() {
  $items = array();
  $items['rules_ccloud_delete_feed_item'] = entity_import('rules_config', '{ "rules_ccloud_delete_feed_item" : {
      "LABEL" : "CCloud: Delete feed item",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "entity_created" : { "node_operation" : "Node operation" } },
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "ccloud_node_operation",
                    "param_title" : "[node:title]",
                    "param_author" : [ "site:current-user" ]
                  },
                  "PROVIDE" : { "entity_created" : { "node_operation" : "Node operation" } }
                }
              },
              { "data_set" : { "data" : [ "node-operation:field-operation" ], "value" : "D" } },
              { "data_set" : {
                  "data" : [ "node-operation:field-id-operated" ],
                  "value" : [ "node:nid" ]
                }
              },
              { "data_set" : {
                  "data" : [ "node-operation:title" ],
                  "value" : "[[node-operation:field-operation]] [node:title]"
                }
              },
              { "data_set" : { "data" : [ "node-operation:status" ], "value" : "0" } }
            ],
            "LABEL" : "Create node operation"
          }
        },
        { "RULE" : {
            "DO" : [ { "entity_delete" : { "data" : [ "node" ] } } ],
            "LABEL" : "Delete node"
          }
        }
      ]
    }
  }');
  $items['rules_ccloud_import_feed_item'] = entity_import('rules_config', '{ "rules_ccloud_import_feed_item" : {
      "LABEL" : "CCloud: Import feed item",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "createcloud_feeds" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "DO" : [ { "ccloud_feeds_import" : { "node" : [ "node" ] } } ],
            "LABEL" : "Import"
          }
        }
      ]
    }
  }');
  $items['rules_ccloud_update_feed_item'] = entity_import('rules_config', '{ "rules_ccloud_update_feed_item" : {
      "LABEL" : "CCloud: Update feed item",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "entity_created" : { "node_operation" : "Node operation" } },
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "ccloud_node_operation",
                    "param_title" : "[node:title]",
                    "param_author" : [ "site:current-user" ]
                  },
                  "PROVIDE" : { "entity_created" : { "node_operation" : "Node operation" } }
                }
              },
              { "data_set" : { "data" : [ "node-operation:field-operation" ], "value" : "U" } },
              { "data_set" : {
                  "data" : [ "node-operation:field-id-operated" ],
                  "value" : [ "node:nid" ]
                }
              },
              { "data_set" : {
                  "data" : [ "node-operation:title" ],
                  "value" : "[[node-operation:field-operation]] [node:title]"
                }
              },
              { "data_set" : { "data" : [ "node-operation:status" ], "value" : "0" } }
            ],
            "LABEL" : "Create node operation"
          }
        }
      ]
    }
  }');
  return $items;
}
