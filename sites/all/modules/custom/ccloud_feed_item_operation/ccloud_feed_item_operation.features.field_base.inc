<?php
/**
 * @file
 * ccloud_feed_item_operation.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ccloud_feed_item_operation_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_id_operated'
  $field_bases['field_id_operated'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_id_operated',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_operation'
  $field_bases['field_operation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_operation',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'U' => '更新',
        'D' => '删除',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
