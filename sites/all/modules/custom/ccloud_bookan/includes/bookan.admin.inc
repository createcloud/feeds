<?php

function ccloud_bookan_settings() {
  $prefix = CCLOUD_BOOKAN_PREFIX;
  $api_prefix = CCLOUD_BOOKAN_API_PREFIX;

  $form['bookan_class_refresh'] = array(
    '#type' => 'submit',
    '#value' => t('更新期刊分类'),
    '#submit' => array('ccloud_bookan_class_refresh'),
  );

  $form['bookan_classlist_refresh'] = array(
    '#type' => 'submit',
    '#value' => t('更新期刊品种'),
    '#submit' => array('ccloud_bookan_classlist_refresh'),
  );

  $form['bookan_issue_refresh'] = array(
    '#type' => 'submit',
    '#value' => t('更新期刊'),
    '#submit' => array('ccloud_bookan_issue_refresh_all'),
  );

  $form['bookan_issue_refresh_missing_files'] = array(
    '#type' => 'submit',
    '#value' => t('更新期刊缺失文件'),
    '#submit' => array('ccloud_bookan_issue_refresh_missing_files'),
  );

  $form['bookan_links'] = array(
    '#theme' => 'item_list',
    '#items' => array(
      l('杂志列表', 'admin/config/services/bookan/magazine'),
      l('期刊列表', 'admin/config/services/bookan/issue'),
    ),
  );

  $form['bookan_server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server information'),
  );

  $server_elements = array(
    'business_cache_server' => array(
      'title' => 'Business cache server',
      'default' => 'http://183.63.187.14/businesscache',
    ),
    'logo_image_server' => array(
      'title' => 'Logo image server',
      'default' => 'http://183.63.187.14/businesscache',
    ),
    'page_server' => array(
      'title' => 'Page server',
      'default' => 'http://183.63.187.14',
    ),
  );

  foreach ($server_elements as $element_key => $element_info) {
    $element_id = $prefix . $element_key;

    $form['bookan_server'][$element_id] = array(
      '#type' => 'textfield',
      '#title' => t($element_info['title']),
      '#description' => t('Default: ' . $element_info['default']),
      '#default_value' => variable_get($element_id, $element_info['default']),
      '#size' => 100,
    );
  }

  $form['bookan_org'] = array(
    '#type' => 'fieldset',
    '#title' => t('Organization information'),
  );

  $element_id = $prefix . 'orgid';
  $form['bookan_org'][$element_id] = array(
    '#type' => 'textfield',
    '#title' => t('Organization code'),
    '#description' => t('Provided by bookan for resource access'),
    '#default_value' => variable_get($element_id, ''),
    '#size' => 50,
  );

  /*
  $element_id = $prefix . 'org_name';
  $form['bookan_org'][$element_id] = array(
    '#type' => 'item',
    '#title' => variable_get($element_id, ''),
  );

  $element_id = $prefix . 'org_slogan';
  $form['bookan_org'][$element_id] = array(
    '#type' => 'item',
    '#title' => variable_get($element_id, ''),
  );

  $element_id = $prefix . 'org_logo';
  $form['bookan_org'][$element_id] = array(
    '#markup' => '<img src="' . variable_get($element_id, '') . '" />',
  );
  */

  $form['bookan_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API addresses'),
  );

  $api_elements = array(
    'org_info' => array(
      'title' => 'Organization info',
      'default' => '{business_cache_server}/{orgid}/bookan_{orgid}/info/info_{orgid}.txt',
    ),
    'org_logo' => array(
      'title' => 'Organization logo',
      'default' => '{logo_image_server}/{orgid}.png',
    ),
    'class' => array(
      'title' => 'Class',
      'default' => '{business_cache_server}/{orgid}/bookan_{orgid}/class/class_{orgid}.txt',
    ),
    'class_list' => array(
      'title' => 'Class list',
      'default' => '{business_cache_server}/{orgid}/bookan_{orgid}/classlist/classlist_{orgid}_{clasorg}_{page}.txt',
    ),
    'total' => array(
      'title' => 'Class total',
      'default' => '{business_cache_server}/{orgid}/bookan_{orgid}/total/total_{orgid}_{clasorg}.txt',
    ),
    'year' => array(
      'title' => 'Magazine year list',
      'default' => '{business_cache_server}/magazine/magazine_{magazineid}/year/year_{magazineid}.txt',
    ),
    'year_list' => array(
      'title' => 'Magazine list by year',
      'default' => '{business_cache_server}/magazine/magazine_{magazineid}/yearlist/yearlist_{magazineid}_{year}.txt',
    ),
    'cover' => array(
      'title' => 'Magazine issue cover',
      'default' => '{page_server}/{path}/{magazineid}/{magazineid}-{issueid}/cover_{key}.mg',
    ),
    'jpg_cover' => array(
      'title' => 'Magazine issue jpg cover',
      'default' => '{page_server}/{jpath}/{magazineid}/{magazineid}-{issueid}/cover_{key}.jpg',
    ),
    'catalog' => array(
      'title' => 'Magazine issue catalog',
      'default' => '{business_cache_server}/magazine/magazine_{magazineid}/catalog/catalog_{issueid}.txt',
    ),
    'hash' => array(
      'title' => 'Magazine issue page hash',
      'default' => 'http://srv1.magook.com:82/getHash.aspx?issueid={issueid}&maginfoid={magazineid}&page={page}',
    ),
    'page' => array(
      'title' => 'Magazine issue page',
      'default' => '{page_server}/{path}/{magazineid}/{magazineid}-{issueid}/{hash}_{key}.mg',
    ),
    'jpg_page' => array(
      'title' => 'Magazine issue jpg page',
      'default' => '{page_server}/{jpath}/{magazineid}/{magazineid}-{issueid}/{hash}_{key}.jpg',
    ),
    'text_catalog' => array(
      'title' => 'Magazine issue catalog - text version',
      'default' => '{page_server}/ecatalog/{magazineid}/{magazineid}-{issueid}/book.xml',
    ),
    'text_page' => array(
      'title' => 'Magazine issue page - text version',
      'default' => '{page_server}/epath/ebooks/{magazineid}/{magazineid}-{issueid}/{n}.xml',
    ),
  );

  foreach ($api_elements as $element_key => $element_info) {
    $element_id = $api_prefix . $element_key;

    $form['bookan_api'][$element_id] = array(
      '#type' => 'textfield',
      '#title' => t($element_info['title']),
      '#description' => t('Default: ' . $element_info['default']),
      '#default_value' => variable_get($element_id, $element_info['default']),
      '#size' => 100,
    );
  }

  $form['bookan_limit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Issue refresh limits'),
  );

  $limit_elements = array(
    'classlist_refresh_by_class_limit' => array(
      'title' => '每个分类更新多少种杂志',
      'default' => '1',
    ),
    'issue_refresh_by_magazine_limit' => array(
      'title' => '每种杂志更新多少年期刊',
      'default' => '1',
    ),
    'issue_refresh_by_magazine_year_limit' => array(
      'title' => '每年更新多少期期刊',
      'default' => '1',
    ),
  );

  foreach ($limit_elements as $element_key => $element_info) {
    $element_id = 'ccloud_bookan_' . $element_key;

    $form['bookan_limit'][$element_id] = array(
      '#type' => 'textfield',
      '#title' => t($element_info['title']),
      '#description' => t('Default: ' . $element_info['default']),
      '#default_value' => variable_get($element_id, $element_info['default']),
      '#size' => 100,
    );
  }

  $form['#submit'][] = 'ccloud_bookan_settings_submit';

  return system_settings_form($form);
}

function ccloud_bookan_settings_submit($form, &$form_state) {
  $element_id = CCLOUD_BOOKAN_PREFIX . 'orgid';

  $orgid_before = variable_get($element_id, '');
  $orgid_after = $form_state['values'][$element_id];

  if ($orgid_before != $orgid_after) {
    ccloud_bookan_org_info_update($orgid_after);
  }
}

function ccloud_bookan_org_info_update($orgid) {
  $org_info = ccloud_bookan_api_get_org_info($orgid);

  variable_set(CCLOUD_BOOKAN_PREFIX . 'org_name', !empty($org_info['name']) ? $org_info['name'] : t('Name not found'));
  variable_set(CCLOUD_BOOKAN_PREFIX . 'org_slogan', !empty($org_info['slogan']) ? $org_info['slogan'] : t('Slogan not found'));

  $org_logo = ccloud_bookan_api_get_org_logo($orgid);

  variable_set(CCLOUD_BOOKAN_PREFIX . 'org_logo', $org_logo);
}