<?php

function ccloud_bookan_api_token_defaults() {
  $defaults = array();

  $token_ids = array('business_cache_server', 'logo_image_server', 'page_server', 'orgid');

  foreach ($token_ids as $token_id) {
    $defaults[$token_id] = variable_get(CCLOUD_BOOKAN_PREFIX . $token_id);
  }

  return $defaults;
}

function ccloud_bookan_api_token_replace($rule_string, $token_values = array()) {
  $token_defaults = ccloud_bookan_api_token_defaults();
  $token_values = array_replace($token_defaults, $token_values);

  foreach ($token_values as $token => $token_value) {
    $rule_string = str_replace('{' . $token . '}', $token_value, $rule_string);
  }

  return $rule_string;
}

function ccloud_bookan_api_get_data($rule_id, $rule_tokens = array(), $data_type = 'json') {
  $rule = variable_get(CCLOUD_BOOKAN_API_PREFIX . $rule_id, NULL);
  $url = ccloud_bookan_api_token_replace($rule, $rule_tokens);

//  dpm($url, 'url');

  $data = ccloud_bookan_api_send_request($url);

  if ($data_type === 'json') {
    $data = drupal_json_decode($data);
  }

  return $data;
}

function ccloud_bookan_api_send_request($url, $data = array(), $method = 'GET') {
  $options = array(
    'method' => $method,
    'data' => serialize($data),
  );

  $result = drupal_http_request($url, $options);
  $result_data = NULL;

//  dpm($result, 'result');

  if ($result->code === '200' && $result->status_message === 'OK') {
    $result_data = $result->data;
  }

  return $result_data;
}

function ccloud_bookan_api_get_org_info($orgid) {
  return ccloud_bookan_api_get_data('org_info', array('orgid' => $orgid));
}

function ccloud_bookan_api_get_org_logo($orgid) {
  $rule = variable_get(CCLOUD_BOOKAN_API_PREFIX . 'org_logo', NULL);
  return ccloud_bookan_api_token_replace($rule, array('orgid' => $orgid));
}

function ccloud_bookan_api_get_class() {
  $data = ccloud_bookan_api_get_data('class');
  return !empty($data['data']) ? $data['data'] : NULL;
}

function ccloud_bookan_api_get_class_list($clasorg, $page) {
  $data = ccloud_bookan_api_get_data('class_list', array('clasorg' => $clasorg, 'page' => $page));
  return !empty($data['data']) ? $data['data'] : NULL;
}

function ccloud_bookan_api_get_total($clasorg) {
  return ccloud_bookan_api_get_data('total', array('clasorg' => $clasorg));
}

function ccloud_bookan_api_get_year($magazineid) {
  $data = ccloud_bookan_api_get_data('year', array('magazineid' => $magazineid));
  return !empty($data['data']) ? $data['data'] : NULL;
}

function ccloud_bookan_api_get_year_list($magazineid, $year) {
  $data = ccloud_bookan_api_get_data('year_list', array('magazineid' => $magazineid, 'year' => $year));
  return !empty($data['data']) ? $data['data'] : NULL;
}

function ccloud_bookan_api_get_cover($path, $magazineid, $issueid, $key) {
  return ccloud_bookan_api_get_data('cover', array('path' => $path, 'magazineid' => $magazineid, 'issueid' => $issueid, 'key' => $key), 'mg');
}

function ccloud_bookan_api_get_jpg_cover($jpath, $magazineid, $issueid, $key) {
  return ccloud_bookan_api_get_data('jpg_cover', array('jpath' => $jpath, 'magazineid' => $magazineid, 'issueid' => $issueid, 'key' => $key), 'jpg');
}

function ccloud_bookan_api_get_catalog($magazineid, $issueid) {
  $data = ccloud_bookan_api_get_data('catalog', array('magazineid' => $magazineid, 'issueid' => $issueid));
  return !empty($data['data']) ? $data['data'] : NULL;
}

function ccloud_bookan_api_get_hash($magazineid, $issueid, $page) {
  return ccloud_bookan_api_get_data('hash', array('magazineid' => $magazineid, 'issueid' => $issueid, 'page' => $page), 'text');
}

function ccloud_bookan_api_get_page_path($path, $magazineid, $issueid, $hash, $key) {
  $rule = variable_get(CCLOUD_BOOKAN_API_PREFIX . 'page', NULL);
  return ccloud_bookan_api_token_replace($rule, array('path' => $path, 'magazineid' => $magazineid, 'issueid' => $issueid, 'hash' => $hash, 'key' => $key));
}

function ccloud_bookan_api_get_jpg_page_path($jpath, $magazineid, $issueid, $hash, $key) {
  $rule = variable_get(CCLOUD_BOOKAN_API_PREFIX . 'jpg_page', NULL);
  return ccloud_bookan_api_token_replace($rule, array('jpath' => $jpath, 'magazineid' => $magazineid, 'issueid' => $issueid, 'hash' => $hash, 'key' => $key));
}

function ccloud_bookan_api_get_page($path, $magazineid, $issueid, $hash, $key) {
  return ccloud_bookan_api_get_data('page', array('path' => $path, 'magazineid' => $magazineid, 'issueid' => $issueid, 'hash' => $hash, 'key' => $key), 'mg');
}

function ccloud_bookan_api_get_text_catalog($magazineid, $issueid) {
  return ccloud_bookan_api_get_data('text_catalog', array('magazineid' => $magazineid, 'issueid' => $issueid), 'xml');
}

function ccloud_bookan_api_get_text_page($path, $magazineid, $issueid, $n) {
  return ccloud_bookan_api_get_data('text_page', array('path' => $path, 'magazineid' => $magazineid, 'issueid' => $issueid, 'n' => $n), 'xml');
}