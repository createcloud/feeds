<?php

define('CCLOUD_BOOKAN_PREFIX', 'bookan_');
define('CCLOUD_BOOKAN_API_PREFIX', 'bookan_api_');

define('CCLOUD_BOOKAN_CLASS_VOC_NAME', 'ccloud_bookan_class');
define('CCLOUD_BOOKAN_MAGAZINE_TYPE_NAME', 'ccloud_bookan_magazine');
define('CCLOUD_BOOKAN_ISSUE_TYPE_NAME', 'ccloud_bookan_issue');

define('CCLOUD_BOOKAN_IMAGE_KEY_BIG', 'big');
define('CCLOUD_BOOKAN_IMAGE_KEY_SMALL', 'small');

module_load_include('inc', 'ccloud_bookan', 'includes/bookan.api');

/**
 * Implements hook_cron().
 */
function ccloud_bookan_cron() {
  if (variable_get('ccloud_bookan_cron_enabled', TRUE)) {
    ccloud_bookan_queue_job_dispatch();
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function ccloud_bookan_cron_queue_info() {
  $queues = array();

  $queues['ccloud_bookan_issues_import'] = array(
    'worker callback' => 'ccloud_bookan_issues_import',
    'time' => 60,
  );

  $queues['ccloud_bookan_issue_pic_files_import'] = array(
    'worker callback' => 'ccloud_bookan_issue_pic_files_import',
    'time' => 60,
  );

  $queues['ccloud_bookan_issue_txt_files_import'] = array(
    'worker callback' => 'ccloud_bookan_issue_txt_files_import',
    'time' => 60,
  );

  return $queues;
}

function ccloud_bookan_menu() {
  $items = array();

  $items['admin/config/services/bookan'] = array(
    'title' => '博看',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ccloud_bookan_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/bookan.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/services/bookan/classlist/refresh'] = array(
    'page callback' => 'ccloud_bookan_classlist_refresh',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/services/bookan/issue/refresh'] = array(
    'page callback' => 'ccloud_bookan_issue_refresh_all',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/services/bookan/issue/refresh/missing_files'] = array(
    'page callback' => 'ccloud_bookan_issue_refresh_missing_files',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['api/bookan/magazine/filter/%'] = array(
    'page callback' => 'ccloud_bookan_magazine_filter',
    'page arguments' => array(4),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['api/bookan/issue/filter/%'] = array(
    'page callback' => 'ccloud_bookan_issue_filter',
    'page arguments' => array(4),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function ccloud_bookan_magazine_filter($type) {
  $view_result = views_get_view_result('bookan_magazine_list', 'filter_' . $type);
  $result = call_user_func('ccloud_bookan_magazine_filter_' . $type, $view_result);
  drupal_json_output($result);
}

function ccloud_bookan_magazine_filter_text_content($view_result) {
  $result = array();

  foreach ($view_result as $record) {
    $result[] = $record->field_field_ccloud_bookan_magazine_id[0]['raw']['value'];
  }

  return $result;
}

function ccloud_bookan_issue_filter($type, $arg1) {
  $view_result = views_get_view_result('bookan_issue_list', 'filter_' . $type, $arg1);
  $result = call_user_func('ccloud_bookan_issue_filter_' . $type, $view_result);
  drupal_json_output($result);
}

function ccloud_bookan_issue_filter_text_content($view_result) {
  $result = array();

  foreach ($view_result as $record) {
    $result[] = $record->field_field_ccloud_bookan_issue_id[0]['raw']['value'];
  }

  return $result;
}

function ccloud_bookan_get_nid_by_unique_field($field_name, $field_value) {
  $nids = entity_property_query('node', $field_name, $field_value, 1);
  $nid = !empty($nids) ? $nids[0] : NULL;

  return $nid;
}

function ccloud_bookan_class_refresh() {
  $classes = ccloud_bookan_api_get_class();

  if (!empty($classes)) {
    $vocabulary = taxonomy_vocabulary_machine_name_load(CCLOUD_BOOKAN_CLASS_VOC_NAME);

    foreach ($classes as $class) {
      $clasorg = $class['category'];
      $name = $class['name'];

      $class_tids = entity_property_query('taxonomy_term', 'field_ccloud_bookan_clasorg', $clasorg, 1);

      if (empty($class_tids)) {
        $class_data = array(
          'name' => $name,
          'parent' => array(0),
          'vid' => $vocabulary->vid,
          'weight' => $clasorg,
        );

        $class_term = entity_create('taxonomy_term', $class_data);
        $class_wrapper = entity_metadata_wrapper('taxonomy_term', $class_term);
        $class_wrapper->save();

        $class_wrapper->field_ccloud_bookan_clasorg = $clasorg;
        $class_wrapper->save();
      }
      else {
        $class_term = taxonomy_term_load($class_tids[0]);

        if ($class_term->name != $name) {
          $class_term->name = $name;
          taxonomy_term_save($class_term);
        }
      }
    }
  }

  drupal_set_message(t('期刊分类已更新'));
  return TRUE;
}

function ccloud_bookan_classlist_refresh($form, &$form_state, $enforce = FALSE) {
  // Allow execution to continue even if the request gets canceled.
  @ignore_user_abort(TRUE);

  $vocabulary = taxonomy_vocabulary_machine_name_load(CCLOUD_BOOKAN_CLASS_VOC_NAME);
  $terms = taxonomy_get_tree($vocabulary->vid, 0, NULL, FALSE);

  if (!empty($terms)) {
    foreach ($terms as $term) {
      $wrapper = entity_metadata_wrapper('taxonomy_term', $term->tid);
      $clasorg = $wrapper->field_ccloud_bookan_clasorg->value();

      ccloud_bookan_classlist_refresh_by_class($clasorg, $enforce);
    }
  }

  drupal_set_message(t('期刊品种已更新'));
  return TRUE;
}

function ccloud_bookan_classlist_refresh_by_class($clasorg, $enforce = FALSE) {
  if (!$clasorg) {
    return TRUE;
  }

  $total = ccloud_bookan_api_get_total($clasorg);

  $items = $total['magazine'];
  $items_per_page = 60;

  $mod = $items % $items_per_page;

  $pages = ($items - $mod) / $items_per_page + ($mod > 0 ? 1 : 0);
  $page_min = 1;

  $page = $page_min;

  while ($page <= $pages) {
    $class_list = ccloud_bookan_api_get_class_list($clasorg, $page);

    if (!empty($class_list)) {
      $count = 0;
      $limit = variable_get('ccloud_bookan_classlist_refresh_by_class_limit', 1);

      foreach ($class_list as $issue) {
        $issue['clasorg'] = $clasorg;
        ccloud_bookan_issue_refresh($issue, $enforce, FALSE);

        if (++$count >= $limit) {
          return TRUE;
        }
      }
    }

    $page++;
  }

  return TRUE;
}

function ccloud_bookan_issue_refresh_all($form, &$form_state, $enforce = FALSE) {
  // Allow execution to continue even if the request gets canceled.
  @ignore_user_abort(TRUE);

  $vocabulary = taxonomy_vocabulary_machine_name_load(CCLOUD_BOOKAN_CLASS_VOC_NAME);
  $terms = taxonomy_get_tree($vocabulary->vid, 0, NULL, FALSE);

  if (!empty($terms)) {
    foreach ($terms as $term) {
      $wrapper = entity_metadata_wrapper('taxonomy_term', $term->tid);
      $clasorg = $wrapper->field_ccloud_bookan_clasorg->value();

      ccloud_bookan_issue_refresh_by_class($clasorg, $term->tid, $enforce);
    }
  }

  drupal_set_message(t('期刊已更新'));
  return TRUE;
}

function ccloud_bookan_issue_refresh_by_class($clasorg, $class_tid, $enforce = FALSE) {
  $magazine_nids = entity_property_query('node', 'field_ccloud_bookan_class', $class_tid, 99999);

  if (!empty($magazine_nids)) {
    foreach ($magazine_nids as $magazine_nid) {
      $magazine_wrapper = entity_metadata_wrapper('node', $magazine_nid);
      ccloud_bookan_issue_refresh_by_magazine($magazine_wrapper->field_ccloud_bookan_magazine_id->value(), $enforce);
    }
  }

  return TRUE;
}

function ccloud_bookan_issue_refresh_by_magazine($magazine_id, $enforce = FALSE) {
  $magazine_nid = ccloud_bookan_get_nid_by_unique_field('field_ccloud_bookan_magazine_id', $magazine_id);

  if (empty($magazine_nid)) {
    return TRUE;
  }
  else {
    $magazine = entity_metadata_wrapper('node', $magazine_nid);
    $status = $magazine->value()->status;
    if (!$status) {
      return TRUE;
    }
  }

  $years = ccloud_bookan_api_get_year($magazine_id);

  if (!empty($years)) {
    $count = 0;
    $limit = variable_get('ccloud_bookan_issue_refresh_by_magazine_limit', 1);

    foreach ($years as $year) {
      ccloud_bookan_issue_refresh_by_magazine_year($magazine_id, $year['year'], $enforce);

      if (++$count >= $limit) {
        return TRUE;
      }
    }
  }

  return TRUE;
}

function ccloud_bookan_issue_refresh_by_magazine_year($magazine_id, $year, $enforce = FALSE) {
  $year_list = ccloud_bookan_api_get_year_list($magazine_id, $year);

  if (!empty($year_list)) {
    $magazine_nid = ccloud_bookan_magazine_refresh(array('magazineid' => $magazine_id));
    $magazine_name = entity_metadata_wrapper('node', $magazine_nid)->title->value();

    $count = 0;
    $limit = variable_get('ccloud_bookan_issue_refresh_by_magazine_year_limit', 2);

    foreach ($year_list as $issue) {
      $issue['magazineid'] = $magazine_id;
      $issue['magazinename'] = $magazine_name;

      ccloud_bookan_issue_refresh($issue, $enforce, FALSE);

      if ((++$count >= $limit) && ($limit > 0)) {
        return TRUE;
      }
    }
  }

  return TRUE;
}

function ccloud_bookan_issue_files_refresh($issue, $types = NULL) {
  if (is_null($types)) {
    $types = array(
      'cover',
      'catalog',
      'text_catalog',
      'text_content',
      'text_archive',
    );
  }

  foreach ($types as $type) {
    call_user_func('ccloud_bookan_issue_' . $type . '_refresh', $issue);
  }
}

function ccloud_bookan_issue_files_refresh_by_nid($nid, $types = NULL) {
  $issue_wrapper = entity_metadata_wrapper('node', $nid);

  $magazine_nid = $issue_wrapper->field_ccloud_bookan_magazine->value()->nid;
  $magazine_wrapper = entity_metadata_wrapper('node', $magazine_nid);

  $issue = array(
    'nid' => $nid,
    'magazineid' => $magazine_wrapper->field_ccloud_bookan_magazine_id->value(),
    'issueid' => $issue_wrapper->field_ccloud_bookan_issue_id->value(),
    'path' => $issue_wrapper->field_ccloud_bookan_path->value(),
    'jpath' => $issue_wrapper->field_ccloud_bookan_jpath->value(),
    'count' => $issue_wrapper->field_ccloud_bookan_count->value(),
    'start' => $issue_wrapper->field_ccloud_bookan_start->value(),
  );

  ccloud_bookan_issue_files_refresh($issue, $types);
}

function ccloud_bookan_issue_refresh($issue, $enforce = FALSE, $refresh_files = TRUE) {
  $issue_nid = ccloud_bookan_get_nid_by_unique_field('field_ccloud_bookan_issue_id', $issue['issueid']);

  if (empty($issue_nid) || $enforce) {
    $magazine = array(
      'magazineid' => $issue['magazineid'],
      'magazinename' => $issue['magazinename'],
      'clasorg' => !empty($issue['clasorg']) ? $issue['clasorg'] : -1,
    );

    $magazine_nid = ccloud_bookan_magazine_refresh($magazine);

    if (empty($issue_nid)) {
      $issue_data = array(
        'type' => CCLOUD_BOOKAN_ISSUE_TYPE_NAME,
        'title' => $issue['magazinename'] . ' (' . $issue['issuename'] . ')',
        'uid' => 1,
      );

      $issue_node = entity_create('node', $issue_data);
      $issue_wrapper = entity_metadata_wrapper('node', $issue_node);
      $issue_wrapper->save();
    }
    else {
      $issue_wrapper = entity_metadata_wrapper('node', $issue_nid);
    }

    $year_split = explode('年', $issue['issuename']);
    $year = $year_split[0];
    $count_split = explode('期', $year_split[1]);
    $count = $count_split[0];
    $issue_order = $year * 1000 + $count;

    $issue_wrapper->field_ccloud_bookan_issue_order = $issue_order;
    $issue_wrapper->field_ccloud_bookan_issue_id = $issue['issueid'];
    $issue_wrapper->field_ccloud_bookan_issue_name = $issue['issuename'];
    $issue_wrapper->field_ccloud_bookan_path = $issue['path'];

    if (!empty($issue['jpath'])) {
      $issue_wrapper->field_ccloud_bookan_jpath = $issue['jpath'];
    }

    $issue_wrapper->field_ccloud_bookan_price_0 = $issue['price0'];
    $issue_wrapper->field_ccloud_bookan_price_1 = $issue['price1'];
    $issue_wrapper->field_ccloud_bookan_count = $issue['count'];
    $issue_wrapper->field_ccloud_bookan_start = $issue['start'];
    $issue_wrapper->field_ccloud_bookan_guid = $issue['guid'];
    $issue_wrapper->field_ccloud_bookan_magazine = $magazine_nid;

    if (preg_match('/' . $issue['magazineid'] . '(.*)\d{3}/', $issue['guid'], $matches_pub_date)) {
      $issue_wrapper->field_ccloud_bookan_pub_date = $matches_pub_date[1];
    }

    $issue_wrapper->save();

    if ($refresh_files) {
      $issue['nid'] = $issue_wrapper->nid->value();
      ccloud_bookan_issue_files_refresh($issue);
    }
  }

  return $issue_nid;
}

function ccloud_bookan_issue_cover_refresh($issue, $enforce = FALSE) {
  $magazine_id = $issue['magazineid'];
  $issue_id = $issue['issueid'];
  $path = $issue['path'];
  $jpath = $issue['jpath'];
  $keys = array(CCLOUD_BOOKAN_IMAGE_KEY_SMALL);

  foreach ($keys as $key) {
    $directory = file_build_uri("bookan/issue/$magazine_id/$magazine_id-$issue_id");
    $destination = "$directory/cover_$key.mg";

    if (!file_exists($destination) || $enforce) {
      $data = ccloud_bookan_api_get_cover($path, $magazine_id, $issue_id, $key);
      if (!empty($data)) {
        if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
          file_save_data($data, $destination, FILE_EXISTS_REPLACE);
        }
        else {
          watchdog('ccloud_bookan', 'Fail to save cover file. %directory is not writable.', array('%directory' => $directory), WATCHDOG_ERROR, 'node/'.$issue['nid']);
        }
      }
    }

    $destination = "$directory/cover_$key.jpg";

    if (!file_exists($destination) || $enforce) {
      $data = ccloud_bookan_api_get_jpg_cover($jpath, $magazine_id, $issue_id, $key);
      if (!empty($data)) {
        if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
          file_save_data($data, $destination, FILE_EXISTS_REPLACE);
        }
        else {
          watchdog('ccloud_bookan', 'Fail to save cover file. %directory is not writable.', array('%directory' => $directory), WATCHDOG_ERROR, 'node/'.$issue['nid']);
        }
      }
    }
  }
}

function ccloud_bookan_issue_catalog_refresh($issue) {
  $magazine_id = $issue['magazineid'];
  $issue_id = $issue['issueid'];
  $start = $issue['start'];

  $data = ccloud_bookan_api_get_catalog($magazine_id, $issue_id);
  $content_path = ccloud_bookan_issue_content_refresh($issue);

  if (!empty($data) && !empty($content_path)) {
    /*

    $cmp_func = function($a, $b) {
      return ($a['page'] > $b['page']) ? 1 : (($a['page'] < $b['page']) ? -1 : 0);
    };

    usort($data, $cmp_func);

    */

    foreach ($data as $index => $page_info) {
      if (!empty($content_path[$page_info['page'] + $start])) {
        $data[$index]['path'] = $content_path[$page_info['page'] + $start];
      }
    }

    $directory = file_build_uri("bookan/issue/$magazine_id/$magazine_id-$issue_id");

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $destination = "$directory/image_catalog";
      file_save_data(drupal_json_encode($data), $destination, FILE_EXISTS_REPLACE);
      ccloud_bookan_update_entity_field_value('node', $issue['nid'], 'field_ccloud_bookan_cat_udt', time());
    }
    else {
      watchdog('ccloud_bookan', '%directory is not writable.', array('%directory' => $directory), WATCHDOG_ERROR, 'node/'.$issue['nid']);
    }
  }

  return TRUE;
}

function ccloud_bookan_update_entity_field_value($entity_type, $entity_id, $field_name, $field_value) {
  $entity = entity_metadata_wrapper($entity_type, $entity_id);
  $entity->$field_name->set($field_value);
  $entity->save();
}

function ccloud_bookan_issue_content_refresh($issue) {
  $magazine_id = $issue['magazineid'];
  $issue_id = $issue['issueid'];
  $path = $issue['path'];
  $jpath = $issue['jpath'];
  $count = $issue['count'];
  $keys = array(CCLOUD_BOOKAN_IMAGE_KEY_BIG, CCLOUD_BOOKAN_IMAGE_KEY_SMALL);

//  $mg_content_path = array();
//  $jpg_content_path = array();

  $content_path_with_page = array();

  $page = 1;
  while ($page <= $count) {
    $hash = ccloud_bookan_api_get_hash($magazine_id, $issue_id, $page);

    foreach ($keys as $key) {
      // Get page content path only, not saving image to local server
      $mg_page_path = ccloud_bookan_api_get_page_path($path, $magazine_id, $issue_id, $hash, $key);
      $jpg_page_path = ccloud_bookan_api_get_jpg_page_path($jpath, $magazine_id, $issue_id, $hash, $key);

//      if (ccloud_bookan_url_exists($mg_page_path)) {
//      $mg_content_path[$page][$key] = $mg_page_path;
//      }

//      if (ccloud_bookan_url_exists($jpg_page_path)) {
//      $jpg_content_path[$page][$key] = $jpg_page_path;
//      }

      $content_path_with_page[$page][$key] = $mg_page_path;
      $content_path_with_page[$page]['jpg_' . $key] = $jpg_page_path;

      /*

      // Get page content, saving image to local server and rename
      $data = ccloud_bookan_api_get_page($path, $magazine_id, $issue_id, $hash, $key);

      if (!empty($data)) {
        if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
          $destination = "$directory/{$page}_$key.webp";
          file_save_data($data, $destination, FILE_EXISTS_REPLACE);

          $content_path[$page][$key] = file_create_url($destination);
        }
      }

      */
    }

    $page++;
  }

  if (!empty($content_path_with_page)) {
//  if (!empty($mg_content_path) && !empty($jpg_content_path)) {
//    $content_path = array(
//      'mg' => array_values($mg_content_path),
//      'jpg' => array_values($jpg_content_path),
//    );
//
//    $content_path_with_page = array(
//      'mg' => $mg_content_path,
//      'jpg' => $jpg_content_path,
//    );

    $directory = file_build_uri("bookan/issue/$magazine_id/$magazine_id-$issue_id");

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $destination = "$directory/image_content_path";
      file_save_data(drupal_json_encode(array_values($content_path_with_page)), $destination, FILE_EXISTS_REPLACE);

      $destination = "$directory/image_content_path_with_page";
      file_save_data(drupal_json_encode($content_path_with_page), $destination, FILE_EXISTS_REPLACE);

      ccloud_bookan_update_entity_field_value('node', $issue['nid'], 'field_ccloud_bookan_con_udt', time());
    }
    else {
      watchdog('ccloud_bookan', 'Fail to save content. %directory is not writable.', array('%directory' => $directory), WATCHDOG_ERROR, 'node/'.$issue['nid']);
    }
  }

  return $content_path_with_page;
}

function ccloud_bookan_issue_text_catalog_refresh($issue) {
  $magazine_id = $issue['magazineid'];
  $issue_id = $issue['issueid'];

  $data = ccloud_bookan_api_get_text_catalog($magazine_id, $issue_id);

  if (!empty($data)) {
    $directory = file_build_uri("bookan/issue/$magazine_id/$magazine_id-$issue_id/text");

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      $destination = "$directory/catalog.xml";
      file_save_data($data, $destination, FILE_EXISTS_REPLACE);
      ccloud_bookan_update_entity_field_value('node', $issue['nid'], 'field_ccloud_bookan_text_cat_udt', time());
    }
    else {
      watchdog('ccloud_bookan', 'Fail to save text catalog. %directory is not writable.', array('%directory' => $directory), WATCHDOG_ERROR, 'node/'.$issue['nid']);
    }
  }

  return TRUE;
}

function ccloud_bookan_issue_text_content_refresh($issue) {
  $magazine_id = $issue['magazineid'];
  $issue_id = $issue['issueid'];
  $path = $issue['path'];

  $data = ccloud_bookan_api_get_text_catalog($magazine_id, $issue_id);

  if (!empty($data)) {
    $dom = new DOMDocument();
    $dom->loadXML($data);
    $xpath = new DOMXPath($dom);
    $query = $xpath->query('//article');

    if ($query && $query->length > 0) {
      $content_updated = FALSE;

      $index = 0;
      while ($index < $query->length) {
        $lm = $query->item($index)->attributes->getNamedItem('lm')->nodeValue;
        $title = $query->item($index)->attributes->getNamedItem('title')->nodeValue;
        $order = $query->item($index)->attributes->getNamedItem('order')->nodeValue;

        $page_data = ccloud_bookan_api_get_text_page($path, $magazine_id, $issue_id, $order);

        if (!empty($page_data)) {
          $directory = file_build_uri("bookan/issue/$magazine_id/$magazine_id-$issue_id/text/content");

          if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
            $destination = "$directory/$order.xml";
            file_save_data($page_data, $destination, FILE_EXISTS_REPLACE);

            $page_data = ccloud_bookan_issue_text_content_extract($page_data);
            $destination = "$directory/$order.txt";
            file_save_data($page_data, $destination, FILE_EXISTS_REPLACE);

            $content_updated = TRUE;
          }
          else {

          }
        }

        $index++;
      }

      if ($content_updated) {
        ccloud_bookan_update_entity_field_value('node', $issue['nid'], 'field_ccloud_bookan_text_con_udt', time());
      }
    }
  }

  return TRUE;
}

function ccloud_bookan_issue_text_archive_refresh($issue) {
  $result = FALSE;

  $magazine_id = $issue['magazineid'];
  $issue_id = $issue['issueid'];

  $name = "$magazine_id-$issue_id";
  $directory = drupal_realpath(file_build_uri("bookan/issue/$magazine_id/$name"));

  $zip = new ZipArchive();
  if ($zip->open("$directory/$name-text.zip", ZipArchive::CREATE)) {
    $zip->addFile("$directory/text/catalog.xml", "$name-text/catalog.xml");
    $zip->addGlob("$directory/text/content/*.*", GLOB_BRACE, array('add_path' => "$name-text/content/", 'remove_all_path' => TRUE));
    $zip->close();

    ccloud_bookan_update_entity_field_value('node', $issue['nid'], 'field_ccloud_bookan_text_arc_udt', time());

    $result = TRUE;
  }

  return $result;
}

function ccloud_bookan_magazine_refresh($magazine) {
  $magazine_nid = ccloud_bookan_get_nid_by_unique_field('field_ccloud_bookan_magazine_id', $magazine['magazineid']);

  if (empty($magazine_nid)) {
    $magazine_data = array(
      'type' => CCLOUD_BOOKAN_MAGAZINE_TYPE_NAME,
      'title' => $magazine['magazinename'],
      'uid' => 1,
    );

    $magazine_node = entity_create('node', $magazine_data);
    $magazine_wrapper = entity_metadata_wrapper('node', $magazine_node);
    $magazine_wrapper->save();

    $class_tids = entity_property_query('taxonomy_term', 'field_ccloud_bookan_clasorg', $magazine['clasorg'], 1);
    if (!empty($class_tids)) {
      $magazine_wrapper->field_ccloud_bookan_class = $class_tids[0];
    }

    $magazine_wrapper->field_ccloud_bookan_magazine_id = $magazine['magazineid'];
    $magazine_wrapper->save();

    $magazine_nid = $magazine_wrapper->nid->value();
  }

  return $magazine_nid;
}

function ccloud_bookan_url_exists($url) {
  $curl = curl_init();
  curl_setopt ($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_NOBODY, TRUE);
  curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, 3);
  curl_setopt($curl, CURLOPT_TIMEOUT, 3);
  curl_exec($curl);

  $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

  curl_close($curl);

  if($http_code == 200) {
    $exists = TRUE;
  }
  else {
    $exists = FALSE;
  }

  return $exists;
}

/**
 * Implements hook_rules_action_info().
 */
function ccloud_bookan_rules_action_info() {
  return array(
    'ccloud_bookan_issue_files_refresh' => array(
      'label' => t('Issue files refresh'),
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Node')),
      ),
      'group' => t('CCloud'),
      'base' => 'rules_action_ccloud_bookan_issue_files_refresh',
    ),
  );
}

function rules_action_ccloud_bookan_issue_files_refresh($node) {
  ccloud_bookan_issue_files_refresh_by_nid($node->nid);
}

function ccloud_bookan_issue_refresh_missing_files() {
  // Allow execution to continue even if the request gets canceled.
  @ignore_user_abort(TRUE);

  $view_result = views_get_view_result('bookan_issue_list', 'missing_files');

  foreach ($view_result as $record) {
    ccloud_bookan_issue_files_refresh_by_nid($record->nid);
  }

  drupal_set_message(t('期刊缺失文件已更新'));
  return TRUE;
}

function ccloud_bookan_issue_text_content_extract($input) {
  // extract <content>
  preg_match('/<content[^>]*>(.*)<\/content>/s', $input, $matches);
  $str = $matches[1];

  // strip all tags except <p>
  $str = strip_tags($str, '<p>');

  // trim white spaces & blank lines
  $str = preg_replace('/(<p[^>]*>)(?:\s|\&nbsp\;|　|\xc2\xa0)*/', '$1', $str);
  $str = str_replace('<p></p>', '', $str);

  // add \n for next step
  $str = str_replace('</p>', "</p>\n", $str);

  // extract all text inside <p>
  preg_match_all('/<p[^>]*>(.*)<\/p>/m', $str, $matches);

  $output = '';
  foreach ($matches[1] as $para) {
    $output .= '    ' . $para . "\n\n";
  }

  return $output;
}

function ccloud_bookan_queue_job_dispatch() {

  // dispatch magazine issues update jobs
  $issue_last_run = variable_get('ccloud_bookan_issues_dispatch_last_run', false);
  if (strtotime('+1 hour', $issue_last_run) <= REQUEST_TIME || !$issue_last_run) {
    ccloud_bookan_queue_issues_job_dispatch();
  }

  // dispatch issue picture files update jobs
  $issue_last_run = variable_get('ccloud_bookan_pic_files_dispatch_last_run', false);
  if (strtotime('+1 hour', $issue_last_run) <= REQUEST_TIME || !$issue_last_run) {
    ccloud_bookan_queue_pic_files_job_dispatch();
  }

  // dispatch issue text files update jobs
  $txt_last_run = variable_get('ccloud_bookan_txt_files_dispatch_last_run', false);
  if (strtotime('+1 day', $txt_last_run) <= REQUEST_TIME || !$txt_last_run) {
    ccloud_bookan_queue_txt_files_job_dispatch();
  }
}

function ccloud_bookan_queue_issues_job_dispatch() {
  $result = db_select('field_data_field_ccloud_bookan_magazine_id', 'f')
    ->condition('f.bundle', 'ccloud_bookan_magazine')
    ->fields('f', array('entity_id', 'field_ccloud_bookan_magazine_id_value'))
    ->execute()->fetchAllKeyed();

  $nids = db_select('queue', 'q')
    ->fields('q', array('entity_id'))
    ->condition('name', 'ccloud_bookan_issues_import')
    ->condition('entity_type', 'node')
    ->execute()->fetchCol();

  $year = variable_get('ccloud_bookan_queue_issue_year', date('Y'));
  $queue = DrupalQueue::get('ccloud_bookan_issues_import');
  $i = 0;
  foreach ($result as $nid => $mag_id) {
    if (!in_array($nid, $nids)) {
      $job = array('nid' => $nid, 'magazine_id' => $mag_id, 'year' => $year, 'enforce' => FALSE);
      if ($queue->createItem($job)) {
        $i++;
      }
    }
  }
  variable_set('ccloud_bookan_issues_dispatch_last_run', REQUEST_TIME);
  watchdog('ccloud_bookan', '!num issues import jobs are created.', array('!num' => $i));
}

function ccloud_bookan_queue_pic_files_job_dispatch() {
  $query = db_select('node', 'n')->condition('n.type', CCLOUD_BOOKAN_ISSUE_TYPE_NAME);
  $query->leftJoin('field_data_field_ccloud_bookan_cat_udt', 'cat', 'n.nid = cat.entity_id');
  $query->leftJoin('field_data_field_ccloud_bookan_con_udt', 'con', 'n.nid = con.entity_id');
  $or = db_or()
    ->isNull('cat.field_ccloud_bookan_cat_udt_value')
    ->isNull('con.field_ccloud_bookan_con_udt_value');
  $query->condition($or);
  $query->addField('n','nid');
  $query->addField('cat','entity_id', 'cat');
  $query->addField('con','entity_id', 'con');
  $rows = $query->execute()->fetchAllAssoc('nid', PDO::FETCH_ASSOC);

  $nids = db_select('queue', 'q')
    ->fields('q', array('entity_id'))
    ->condition('name', 'ccloud_bookan_issue_pic_files_import')
    ->condition('entity_type', 'node')
    ->execute()->fetchCol();

  $queue = DrupalQueue::get('ccloud_bookan_issue_pic_files_import');
  $i = 0;
  foreach ($rows as $nid => $row) {
    if (!in_array($nid, $nids)) {
      if ($queue->createItem($row)) {
        $i++;
      }
    }
  }

  variable_set('ccloud_bookan_pic_files_dispatch_last_run', REQUEST_TIME);
  watchdog('ccloud_bookan', '!num pic files import jobs are created.', array('!num' => $i));
}

function ccloud_bookan_queue_txt_files_job_dispatch() {
  $query = db_select('node', 'n')->condition('n.type', CCLOUD_BOOKAN_ISSUE_TYPE_NAME);
  $query->leftJoin('field_data_field_ccloud_bookan_text_cat_udt', 'cat', 'n.nid = cat.entity_id');
  $query->leftJoin('field_data_field_ccloud_bookan_text_con_udt', 'con', 'n.nid = con.entity_id');
  $or = db_or()
    ->isNull('cat.field_ccloud_bookan_text_cat_udt_value')
    ->isNull('con.field_ccloud_bookan_text_con_udt_value');
  $query->condition($or);
  $query->addField('n','nid');
  $query->addField('cat','entity_id', 'cat');
  $query->addField('con','entity_id', 'con');
  $rows = $query->execute()->fetchAllAssoc('nid', PDO::FETCH_ASSOC);

  $nids = db_select('queue', 'q')
    ->fields('q', array('entity_id'))
    ->condition('name', 'ccloud_bookan_issue_txt_files_import')
    ->condition('entity_type', 'node')
    ->execute()->fetchCol();

  $queue = DrupalQueue::get('ccloud_bookan_issue_txt_files_import');
  $i = 0;
  foreach ($rows as $nid => $row) {
    if (!in_array($nid, $nids)) {
      if ($queue->createItem($row)) {
        $i++;
      }
    }
  }

  variable_set('ccloud_bookan_txt_files_dispatch_last_run', REQUEST_TIME);
  watchdog('ccloud_bookan', '!num text files import jobs are created.', array('!num' => $i));
}

/**
 * Queue worker callback
 *
 * @see ccloud_bookan_cron_queue_info().
 */
function ccloud_bookan_issues_import($job) {
  ccloud_bookan_issue_refresh_by_magazine_year($job['magazine_id'], $job['year'], $job['enforce']);
}

/**
 * Queue worker callback
 *
 * @see ccloud_bookan_cron_queue_info().
 */
function ccloud_bookan_issue_pic_files_import($job) {
  $types = array('cover', 'catalog');
  ccloud_bookan_issue_files_refresh_by_nid($job['nid'], $types);
}

/**
 * Queue worker callback
 *
 * @see ccloud_bookan_cron_queue_info().
 */
function ccloud_bookan_issue_txt_files_import($job) {
  $types = array('text_catalog', 'text_content', 'text_archive');
  ccloud_bookan_issue_files_refresh_by_nid($job['nid'], $types);
}

class CCloudBookanQueue extends SystemQueue {

  /**
   * Overrides parent::createItem()
   */
  public function createItem($data) {

    /*$existing = db_query("select count(item_id) from {queue} where name = :name and data = :data",
      array(':name' => $this->name, ':data' => serialize($data)))->fetchField();

    if ($existing) {
      return false;
    }*/

    $fields = array(
      'name' => $this->name,
      'data' => serialize($data),
      'created' => time(),
    );
    if (isset($data['nid'])) {
      $fields['entity_type'] = 'node';
      $fields['entity_id'] = $data['nid'];
    }
    $query = db_insert('queue')
      ->fields($fields);
    return (bool) $query->execute();
  }
}

if (module_exists('queue_ui')) {
  class QueueUICCloudBookanQueue extends QueueUISystemQueue {}
}
