<?php
/**
 * @file
 * ccloud_bookan_feature.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ccloud_bookan_feature_default_rules_configuration() {
  $items = array();
  $items['rules_ccloud_refresh_issue_files'] = entity_import('rules_config', '{ "rules_ccloud_refresh_issue_files" : {
      "LABEL" : "CCloud: Refresh issue files",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "ccloud_bookan" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "DO" : [ { "ccloud_bookan_issue_files_refresh" : { "node" : [ "node" ] } } ],
            "LABEL" : "Refresh"
          }
        }
      ]
    }
  }');
  return $items;
}
