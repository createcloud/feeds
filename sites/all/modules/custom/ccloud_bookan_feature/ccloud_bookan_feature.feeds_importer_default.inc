<?php
/**
 * @file
 * ccloud_bookan_feature.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_bookan_feature_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'ccloud_bookan_magazine';
  $feeds_importer->config = array(
    'name' => '博看杂志',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '报刊名称',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => '主管单位',
            'target' => 'field_ccloud_bookan_institution',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '国际刊号',
            'target' => 'field_ccloud_bookan_issn',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '刊期',
            'target' => 'field_ccloud_bookan_frequency',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '创刊时间',
            'target' => 'field_ccloud_bookan_started_pub',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'ccloud_bookan_magazine',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['ccloud_bookan_magazine'] = $feeds_importer;

  return $export;
}
