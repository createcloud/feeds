<?php
/**
 * @file
 * ccloud_bookan_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ccloud_bookan_feature_taxonomy_default_vocabularies() {
  return array(
    'ccloud_bookan_class' => array(
      'name' => '博看期刊分类',
      'machine_name' => 'ccloud_bookan_class',
      'description' => 'Bookan class',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
