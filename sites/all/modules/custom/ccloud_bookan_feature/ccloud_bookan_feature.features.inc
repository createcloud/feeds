<?php
/**
 * @file
 * ccloud_bookan_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_bookan_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ccloud_bookan_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ccloud_bookan_feature_node_info() {
  $items = array(
    'ccloud_bookan_issue' => array(
      'name' => t('博看期刊'),
      'base' => 'node_content',
      'description' => t('Bookan issue'),
      'has_title' => '1',
      'title_label' => t('期刊名称'),
      'help' => '',
    ),
    'ccloud_bookan_magazine' => array(
      'name' => t('博看杂志'),
      'base' => 'node_content',
      'description' => t('Bookan magazine'),
      'has_title' => '1',
      'title_label' => t('杂志名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
