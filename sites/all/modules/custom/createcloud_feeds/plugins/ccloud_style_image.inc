<?php

/**
 * @file
 * Style handling for image.
 */

$plugin = array(
  'form' => 'feeds_tamper_ccloud_style_image_form',
  'callback' => 'feeds_tamper_ccloud_style_image_callback',
  'name' => 'CCloud: Style image',
  'multi' => 'loop',
  'category' => 'HTML',
);

function feeds_tamper_ccloud_style_image_form($importer, $element_key, $settings) {
  $form = array();

  $style_options = array();
  $styles = image_styles();
  foreach ($styles as $style => $style_info) {
    $style_options[$style] = $style_info['label'];
  }

  $form['style'] = array(
    '#type' => 'radios',
    '#title' => t('Image style'),
    '#default_value' => isset($settings['style']) ? $settings['style'] : NULL,
    '#options' => $style_options,
    '#required' => TRUE,
  );

  return $form;
}

function feeds_tamper_ccloud_style_image_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  $info = pathinfo($field);
  if (empty($info['extension'])) {
    $field = '';
    return;
  }

  $image_response = drupal_http_request($field);
  if ($image_response->code === '200' && $image_response->status_message === 'OK') {
    $image_data = $image_response->data;

    $directory = file_default_scheme() . '://temp';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $destination = $directory . '/' . createcloud_feeds_remote_file_rename($info['basename']);
    $image_file = file_save_data($image_data, $destination);

    $image = image_load($image_file->uri);
    if (!$image) {
      $field = '';
      return;
    }

    $styles = image_styles();
    $style = $settings['style'];
    $style = !empty($styles[$style]) ? $styles[$style] : array('effects' => array());
    foreach ($style['effects'] as $effect) {
      image_effect_apply($image, $effect);
    }

    image_save($image);
    $field = file_create_url($image->source);
  }
  else {
    $field = '';
  }
}