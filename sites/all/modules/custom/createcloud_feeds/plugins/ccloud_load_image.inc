<?php

/**
 * @file
 * Special handling for remote image tags in a string.
 */

$plugin = array(
  'form' => 'feeds_tamper_ccloud_load_image_form',
  'callback' => 'feeds_tamper_ccloud_load_image_callback',
  'name' => 'CCloud: Load remote image',
  'multi' => 'loop',
  'category' => 'HTML',
);

function feeds_tamper_ccloud_load_image_form($importer, $element_key, $settings) {
  $form = array();
  $form['help'] = array(
    '#markup' => t('Special handling for remote image tags in a string.'),
  );
  return $form;
}

function feeds_tamper_ccloud_load_image_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  if (preg_match_all("/<img[^>]+>/i", $field, $img_match)) {
    $src_prefix = url('load_image', array('absolute' => TRUE, 'query' => array('url' => '')));

    $imgs = $img_match[0];
    foreach ($imgs as $img) {
      if (preg_match('/(src)="([^"]*)"/i', $img, $src_match)) {
        $img_src = $src_match[2];
        $field = str_replace($img_src, $src_prefix . $img_src, $field);
      }
    }
  }
}