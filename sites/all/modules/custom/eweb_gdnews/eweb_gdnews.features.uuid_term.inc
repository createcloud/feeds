<?php
/**
 * @file
 * eweb_gdnews.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function eweb_gdnews_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '资讯',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => '3e15395d-af22-43e7-b21d-a028d3b33eb0',
    'vocabulary_machine_name' => 'gdnews_type',
  );
  $terms[] = array(
    'name' => '动态',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => '5422e579-c930-4e85-a5b6-335c7dac3c9c',
    'vocabulary_machine_name' => 'gdnews_type',
  );
  /*$terms[] = array(
    'name' => '演出',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => '5a0dca79-ed1a-47c7-ace5-a300e684aac8',
    'vocabulary_machine_name' => 'gdnews_type',
  );*/
  $terms[] = array(
    'name' => '演艺',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => '89421e16-9bd6-48cd-8289-05f69518e679',
    'vocabulary_machine_name' => 'gdnews_type',
  );
  $terms[] = array(
    'name' => '展览',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => 'b2a0170e-f717-4768-a93a-846864749ee0',
    'vocabulary_machine_name' => 'gdnews_type',
  );
  $terms[] = array(
    'name' => '讲座',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => 'b59c6dec-6408-4ce8-842e-ff8721f6df14',
    'vocabulary_machine_name' => 'gdnews_type',
  );
  $terms[] = array(
    'name' => '公告',
    'description' => '',
    'format' => 'source_code',
    'weight' => 0,
    'uuid' => 'fa2b53f9-db05-4711-9b40-3f4864f7732b',
    'vocabulary_machine_name' => 'gdnews_type',
  );
  return $terms;
}
