<?php
/**
 * @file
 * eweb_gdnews.features.inc
 */

/**
 * Implements hook_node_info().
 */
function eweb_gdnews_node_info() {
  $items = array(
    'seed' => array(
      'name' => t('Seed'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
