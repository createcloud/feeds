<?php
/**
 * @file
 * ccloud_zslib_api.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ccloud_zslib_api_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ccloud_show_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'CCloud show list';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Order id */
  $handler->display->display_options['fields']['field_ccloud_show_order_id']['id'] = 'field_ccloud_show_order_id';
  $handler->display->display_options['fields']['field_ccloud_show_order_id']['table'] = 'field_data_field_ccloud_show_order_id';
  $handler->display->display_options['fields']['field_ccloud_show_order_id']['field'] = 'field_ccloud_show_order_id';
  $handler->display->display_options['fields']['field_ccloud_show_order_id']['label'] = 'order_id';
  $handler->display->display_options['fields']['field_ccloud_show_order_id']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Show name en */
  $handler->display->display_options['fields']['field_ccloud_show_name_en']['id'] = 'field_ccloud_show_name_en';
  $handler->display->display_options['fields']['field_ccloud_show_name_en']['table'] = 'field_data_field_ccloud_show_name_en';
  $handler->display->display_options['fields']['field_ccloud_show_name_en']['field'] = 'field_ccloud_show_name_en';
  $handler->display->display_options['fields']['field_ccloud_show_name_en']['label'] = 'name_en';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php_1']['id'] = 'php_1';
  $handler->display->display_options['fields']['php_1']['table'] = 'views';
  $handler->display->display_options['fields']['php_1']['field'] = 'php';
  $handler->display->display_options['fields']['php_1']['label'] = 'start_date';
  $handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_1']['php_value'] = '$nid = $row->nid;
$node_wrapper = entity_metadata_wrapper(\'node\', $nid);
$start_date = $node_wrapper->field_ccloud_show_start_date->value();
return $start_date;';
  $handler->display->display_options['fields']['php_1']['php_output'] = '<?php
  print $value;
?>';
  $handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php_2']['id'] = 'php_2';
  $handler->display->display_options['fields']['php_2']['table'] = 'views';
  $handler->display->display_options['fields']['php_2']['field'] = 'php';
  $handler->display->display_options['fields']['php_2']['label'] = 'end_date';
  $handler->display->display_options['fields']['php_2']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_2']['php_value'] = '$nid = $row->nid;
$node_wrapper = entity_metadata_wrapper(\'node\', $nid);
$end_date = $node_wrapper->field_ccloud_show_end_date->value();
return $end_date;';
  $handler->display->display_options['fields']['php_2']['php_output'] = '<?php
  print $value;
?>';
  $handler->display->display_options['fields']['php_2']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_2']['php_click_sortable'] = '';
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = 'pics';
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_value'] = '$nid = $row->nid;
$node_wrapper = entity_metadata_wrapper(\'node\', $nid);
$name_en = $node_wrapper->field_ccloud_show_name_en->value();
$directory = file_build_uri("show/zslib/$name_en");
//return file_create_url("$directory/pics");
return file_get_contents("$directory/pics");';
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
  print $value;
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Sort criterion: Content: Order id (field_ccloud_show_order_id) */
  $handler->display->display_options['sorts']['field_ccloud_show_order_id_value']['id'] = 'field_ccloud_show_order_id_value';
  $handler->display->display_options['sorts']['field_ccloud_show_order_id_value']['table'] = 'field_data_field_ccloud_show_order_id';
  $handler->display->display_options['sorts']['field_ccloud_show_order_id_value']['field'] = 'field_ccloud_show_order_id_value';
  $handler->display->display_options['sorts']['field_ccloud_show_order_id_value']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ccloud_show' => 'ccloud_show',
  );

  /* Display: api */
  $handler = $view->new_display('page', 'api', 'api');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'views_json';
  $handler->display->display_options['style_options']['root_object'] = '';
  $handler->display->display_options['style_options']['top_child_object'] = '';
  $handler->display->display_options['style_options']['plaintext_output'] = 1;
  $handler->display->display_options['style_options']['remove_newlines'] = 0;
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['style_options']['using_views_api_mode'] = 0;
  $handler->display->display_options['style_options']['object_arrays'] = 0;
  $handler->display->display_options['style_options']['numeric_strings'] = 0;
  $handler->display->display_options['style_options']['bigint_string'] = 0;
  $handler->display->display_options['style_options']['pretty_print'] = 0;
  $handler->display->display_options['style_options']['unescaped_slashes'] = 0;
  $handler->display->display_options['style_options']['unescaped_unicode'] = 0;
  $handler->display->display_options['style_options']['char_encoding'] = array();
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'api/show';
  $export['ccloud_show_list'] = $view;

  return $export;
}
