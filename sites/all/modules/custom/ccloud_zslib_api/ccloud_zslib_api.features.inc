<?php
/**
 * @file
 * ccloud_zslib_api.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ccloud_zslib_api_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ccloud_zslib_api_node_info() {
  $items = array(
    'ccloud_show' => array(
      'name' => t('展览'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Show name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
