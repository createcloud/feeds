<?php
/**
 * @file
 * ccloud_news_gzlib_gov_cn.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_news_gzlib_gov_cn_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_gzlib_gov_cn';
  $feeds_importer->config = array(
    'name' => '新闻 - 广州图书馆',
    'description' => '广州图书馆',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'content' => array(
            'name' => 'Content',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '1',
          ),
          'author' => array(
            'name' => 'Author',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'publication_date' => array(
            'name' => 'Publication date',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
          'source' => array(
            'name' => 'Source',
            'value' => '',
            'raw' => 1,
            'debug' => 0,
            'weight' => '4',
          ),
          'content_to_remove' => array(
            'name' => 'Content to remove',
            'value' => '',
            'raw' => 1,
            'debug' => 0,
            'weight' => '5',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => './/img[1]',
            'raw' => 1,
            'debug' => 0,
            'weight' => '6',
          ),
        ),
        'context' => array(
          'value' => '//td[@width=\'698\']/table//tr/td[@valign=\'top\']',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'publication_date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'next_page_url',
            'target' => 'field_next_page_url',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'content_to_remove',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'news_gzlib_gov_cn',
      ),
    ),
    'content_type' => 'news_gzlib_gov_cn',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_gzlib_gov_cn'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_list_gzlib_gov_cn';
  $feeds_importer->config = array(
    'name' => '新闻列表 - 广州图书馆',
    'description' => '广州图书馆',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => '.',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '2',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'context' => array(
          'value' => '//table[@width=\'663\']//td[@height=\'26\']/a',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'format' => 'filtered_html',
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'news_gzlib_gov_cn',
      ),
    ),
    'content_type' => 'news_list_gzlib_gov_cn',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_list_gzlib_gov_cn'] = $feeds_importer;

  return $export;
}
