<?php
/**
 * @file
 * ccloud_news_health_ifeng_com.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_health_ifeng_com_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_health_ifeng_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-content-ccloud_remove_content';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'parent:nid' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'image' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
    ),
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_health_ifeng_com-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-content-find_replace_regex';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(style|class|face|lang|id)="[^"]+"/i',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_health_ifeng_com-content-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-content-rewrite';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[parent:nid]
[content]',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Rewrite';
  $export['news_health_ifeng_com-content-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-image-ccloud_fetch_specified_items';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_fetch_specified_items';
  $feeds_tamper->settings = array(
    'indexes' => '0',
    'return_string' => 1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Fetch specified item(s)';
  $export['news_health_ifeng_com-image-ccloud_fetch_specified_items'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_health_ifeng_com-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_health_ifeng_com-parent_nid-php';
  $feeds_tamper->importer = 'news_health_ifeng_com';
  $feeds_tamper->source = 'parent:nid';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$node = node_load($field);
$content = !empty($node->field_content[LANGUAGE_NONE][0][\'value\']) ? $node->field_content[LANGUAGE_NONE][0][\'value\'] : \'\';
return $content;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['news_health_ifeng_com-parent_nid-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_health_ifeng_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_health_ifeng_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_health_ifeng_com-blank_source_1-default_value'] = $feeds_tamper;

  return $export;
}
