<?php
/**
 * @file
 * ccloud_sports_sports_qianlong_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_sports_sports_qianlong_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_sports_sports_qianlong_com_node_info() {
  $items = array(
    'sports_list_sports_qianlong_com' => array(
      'name' => t('体育列表 - 千龙网体育'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sports_sports_qianlong_com' => array(
      'name' => t('体育 - 千龙网体育'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
