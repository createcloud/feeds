<?php
/**
 * @file
 * ccloud_news_mzjylib_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_mzjylib_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_mzjylib_com_node_info() {
  $items = array(
    'news_list_mzjylib_com' => array(
      'name' => t('新闻列表 - 梅州剑英图书馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_mzjylib_com' => array(
      'name' => t('新闻 - 梅州剑英图书馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
