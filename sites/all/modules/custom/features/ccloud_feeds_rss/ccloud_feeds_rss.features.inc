<?php
/**
 * @file
 * ccloud_feeds_rss.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_feeds_rss_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_feeds_rss_node_info() {
  $items = array(
    'feed' => array(
      'name' => t('RSS源'),
      'base' => 'node_content',
      'description' => t('Subscribe to RSS or Atom feeds. Creates nodes of the content type "Feed item" from feed content.'),
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'feed_item' => array(
      'name' => t('资源项'),
      'base' => 'node_content',
      'description' => t('This content type is being used for automatically aggregated content from feeds.'),
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
