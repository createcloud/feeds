<?php
/**
 * @file
 * ccloud_newspaper_stcn.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_newspaper_stcn_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_newspaper_stcn_node_info() {
  $items = array(
    'paper_news_list_stcn' => array(
      'name' => t('报纸新闻列表 - 证券时报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'paper_news_stcn' => array(
      'name' => t('报纸新闻 - 证券时报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
