<?php
/**
 * @file
 * ccloud_newspaper_stcn.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_newspaper_stcn_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'paper_news_list_stcn';
  $feeds_importer->config = array(
    'name' => '报纸新闻列表 - 证券时报',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => 'a',
            'raw' => 1,
            'debug' => 0,
            'weight' => '0',
          ),
          'title' => array(
            'name' => 'Title',
            'value' => 'a',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'section_id' => array(
            'name' => 'Section id',
            'value' => '../../h2/@id',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'section_name' => array(
            'name' => 'Section name',
            'value' => '../../h2/a[1]',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
          'id' => array(
            'name' => 'id',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '4',
          ),
        ),
        'context' => array(
          'value' => '//div[@id=\'listWrap\']//li',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_news_list:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'id',
            'target' => 'field_id',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'section_id',
            'target' => 'field_section_id',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'section_name',
            'target' => 'field_section_name',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'paper_news_stcn',
      ),
    ),
    'content_type' => 'paper_news_list_stcn',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['paper_news_list_stcn'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'paper_news_stcn';
  $feeds_importer->config = array(
    'name' => '报纸新闻 - 证券时报',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'content' => array(
            'name' => 'Content',
            'value' => 'div[@id=\'mainCon\']',
            'raw' => 1,
            'debug' => 0,
            'weight' => '1',
          ),
          'author' => array(
            'name' => 'Author',
            'value' => 'div[@id=\'mainTiile\']/div[@class=\'from\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'publication_date' => array(
            'name' => 'Publication date',
            'value' => 'div[@id=\'mainTiile\']/div[@class=\'from\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
          'source' => array(
            'name' => 'Source',
            'value' => 'div[@id=\'mainTiile\']/div[@class=\'from\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '4',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => 'div[@id=\'mainCon\']//img[1]/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '5',
          ),
          'content_to_remove' => array(
            'name' => 'Content to remove',
            'value' => 'div[@id=\'mainCon\']//script',
            'raw' => 1,
            'debug' => 0,
            'weight' => '6',
          ),
        ),
        'context' => array(
          'value' => '//div[@id=\'mainTxt\']',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'publication_date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'content_to_remove',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'paper_news_stcn',
      ),
    ),
    'content_type' => 'paper_news_stcn',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['paper_news_stcn'] = $feeds_importer;

  return $export;
}
