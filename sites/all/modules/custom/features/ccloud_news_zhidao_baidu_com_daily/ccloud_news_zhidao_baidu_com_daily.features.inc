<?php
/**
 * @file
 * ccloud_news_zhidao_baidu_com_daily.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_zhidao_baidu_com_daily_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_zhidao_baidu_com_daily_node_info() {
  $items = array(
    'news_list_zhidao_baidu_com_daily' => array(
      'name' => t('新闻列表 - 知道日报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_zhidao_baidu_com_daily' => array(
      'name' => t('新闻 - 知道日报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
