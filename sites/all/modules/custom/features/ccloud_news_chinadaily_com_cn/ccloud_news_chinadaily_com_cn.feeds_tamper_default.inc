<?php
/**
 * @file
 * ccloud_news_chinadaily_com_cn.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_chinadaily_com_cn_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_chinadaily_com_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_chinadaily_com_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_chinadaily_com_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_chinadaily_com_cn-blank_source_2-php_image';
  $feeds_tamper->importer = 'news_chinadaily_com_cn';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$img_match = array();
$src_match = array();
$img_url = \'\';
if (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  if (preg_match(\'/(src)="([^"]*)"/i\', $img_match[0], $src_match))
    $img_url = $src_match[2];
}
return $img_url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['news_chinadaily_com_cn-blank_source_2-php_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_chinadaily_com_cn-blank_source_2-rewrite';
  $feeds_tamper->importer = 'news_chinadaily_com_cn';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[content]',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Rewrite';
  $export['news_chinadaily_com_cn-blank_source_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_chinadaily_com_cn-content-absolute_url_content';
  $feeds_tamper->importer = 'news_chinadaily_com_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_chinadaily_com_cn-content-absolute_url_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_chinadaily_com_cn-content-php_content';
  $feeds_tamper->importer = 'news_chinadaily_com_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match_all(\'/(src)="([^"]*)"/i\', $field, $str_match)) {
  $urls = $str_match[2];
  foreach ($urls as $url) {
    if (strpos($url, \'../../\')) {
      $url_parts = explode(\'/\', $url);
      unset($url_parts[3]);
      unset($url_parts[4]);
      unset($url_parts[5]);
      unset($url_parts[6]);
      unset($url_parts[7]);
      $field = str_replace($url, implode(\'/\', $url_parts), $field);
    }
  }
}
return $field;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['news_chinadaily_com_cn-content-php_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_chinadaily_com_cn-blank_source_1-default_value_status';
  $feeds_tamper->importer = 'news_list_chinadaily_com_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_chinadaily_com_cn-blank_source_1-default_value_status'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_chinadaily_com_cn-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_chinadaily_com_cn';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_chinadaily_com_cn-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_chinadaily_com_cn-feed_source-php';
  $feeds_tamper->importer = 'news_list_chinadaily_com_cn';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$a_match = array();
$href_match = array();
$href_url = \'\';
if (preg_match("/<a[^>]+>/i", $field, $a_match)) {
  if (preg_match(\'/(href)="([^"]*)"/i\', $a_match[0], $href_match))
    $href_url = $href_match[2];
}

$href_url_array = explode(\'://\', $href_url);
if (!empty($href_url_array[1])) {
  $scheme = $href_url_array[0] . \'://\';
  $href_url = $href_url_array[1];
  $href_url_array = explode(\'/\', $href_url);
  if (strpos($href_url_array[1], \'_\')) {
    unset($href_url_array[1]);
  }
  $href_url = $scheme . implode(\'/\', $href_url_array);
}

return $href_url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['news_list_chinadaily_com_cn-feed_source-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_chinadaily_com_cn-title-trim_title';
  $feeds_tamper->importer = 'news_list_chinadaily_com_cn';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['news_list_chinadaily_com_cn-title-trim_title'] = $feeds_tamper;

  return $export;
}
