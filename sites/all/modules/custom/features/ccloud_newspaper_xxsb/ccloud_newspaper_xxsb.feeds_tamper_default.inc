<?php
/**
 * @file
 * ccloud_newspaper_xxsb.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_newspaper_xxsb_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_xxsb-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_xxsb';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_xxsb-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_xxsb-section_name-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_xxsb';
  $feeds_tamper->source = 'section_name';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => 'Z
广告
竞彩专版
观图论势
婚尚周刊',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => 'Z',
      1 => '广告',
      2 => '竞彩专版',
      3 => '观图论势',
      4 => '婚尚周刊',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_xxsb-section_name-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_xxsb-title-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_xxsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '导读
广告
时报
分类
公告',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '导读',
      1 => '广告',
      2 => '时报',
      3 => '分类',
      4 => '公告',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_xxsb-title-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_xxsb-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-content-absolute_url';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_xxsb-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-content-php';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match("/<input id=\\"hdSrc\\" type=\\"hidden\\" value=\\"(.*?)\\*@\\*/i", $field, $img_match)) {
  $field = str_replace(\'<img src="#" id="img"\', \'<img src="\' . $img_match[1] . \'" id="img"\', $field);
}

if (preg_match("/\\*@\\*(.*?)>/i", $field, $img_text_match)) {
  $field = str_replace(\'<label id="lblDesc"></label>\', \'<label id="lblDesc">\' . $img_text_match[1] . \'</label>\', $field);
}

return $field;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_xxsb-content-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-image-php';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$field_array = explode(\'*@*\', $field);
$field = \'\';
if (!empty($field_array[0])) {
  $field = \'http://epaper.xxsb.com\' . $field_array[0];
}
return $field;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_xxsb-image-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-image-rewrite';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[image]',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Rewrite';
  $export['paper_news_xxsb-image-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-publication_date-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/\\s/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_xxsb-publication_date-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-publication_date-php';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$date = \'\';
if (preg_match("/日期:\\[(.*?)\\]/i", $field, $text_match)) {
  $date = $text_match[1];
}
return $date;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_xxsb-publication_date-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-source-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/\\s/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_xxsb-source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_xxsb-source-php';
  $feeds_tamper->importer = 'paper_news_xxsb';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$source = \'\';
if (preg_match("/来源:\\[(.*?)\\]/i", $field, $text_match)) {
  $source = $text_match[1];
}
return $source;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_xxsb-source-php'] = $feeds_tamper;

  return $export;
}
