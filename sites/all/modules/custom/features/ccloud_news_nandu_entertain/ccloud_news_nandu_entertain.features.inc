<?php
/**
 * @file
 * ccloud_news_nandu_entertain.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_nandu_entertain_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_nandu_entertain_node_info() {
  $items = array(
    'rss_nandu_entertain' => array(
      'name' => t('RSS - 南都娱乐周刊'),
      'base' => 'node_content',
      'description' => t('Subscribe to RSS or Atom feeds. Creates nodes of the content type "Feed item" from feed content.'),
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
