<?php
/**
 * @file
 * ccloud_news_nandu_entertain.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_nandu_entertain_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_nandu_entertain-blank_source_1-default_value';
  $feeds_tamper->importer = 'rss_nandu_entertain';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['rss_nandu_entertain-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_nandu_entertain-blank_source_2-php';
  $feeds_tamper->importer = 'rss_nandu_entertain';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$img_match = array();
$src_match = array();
$img_url = \'\';
if (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  if (preg_match(\'/(src)="([^"]*)"/i\', $img_match[0], $src_match))
    $img_url = ($src_match[2]);
}

return $img_url;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Extract an image';
  $export['rss_nandu_entertain-blank_source_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_nandu_entertain-blank_source_2-rewrite';
  $feeds_tamper->importer = 'rss_nandu_entertain';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[description]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['rss_nandu_entertain-blank_source_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_nandu_entertain-description-ccloud_load_image';
  $feeds_tamper->importer = 'rss_nandu_entertain';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'ccloud_load_image';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Load remote image';
  $export['rss_nandu_entertain-description-ccloud_load_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_nandu_entertain-description-strip_tags';
  $feeds_tamper->importer = 'rss_nandu_entertain';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<p><a><img><em><strong><h3><h4><h5>',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Strip tags';
  $export['rss_nandu_entertain-description-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_nandu_entertain-timestamp-timetodate';
  $feeds_tamper->importer = 'rss_nandu_entertain';
  $feeds_tamper->source = 'timestamp';
  $feeds_tamper->plugin_id = 'timetodate';
  $feeds_tamper->settings = array(
    'date_format' => 'Y-m-d H:i',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Unix timestamp to Date';
  $export['rss_nandu_entertain-timestamp-timetodate'] = $feeds_tamper;

  return $export;
}
