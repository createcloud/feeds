<?php
/**
 * @file
 * ccloud_paper_news_dnkb.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_paper_news_dnkb_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_paper_news_dnkb_node_info() {
  $items = array(
    'paper_news_dnkb' => array(
      'name' => t('报纸新闻 - 东南快报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'paper_news_list_1_dnkb' => array(
      'name' => t('报纸新闻列表一 - 东南快报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'paper_news_list_dnkb' => array(
      'name' => t('报纸新闻列表 - 东南快报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
