<?php
/**
 * @file
 * ccloud_news_mzswhg_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_mzswhg_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_mzswhg_com_node_info() {
  $items = array(
    'news_list_mzswhg_com' => array(
      'name' => t('新闻列表 - 梅州市文化馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_mzswhg_com' => array(
      'name' => t('新闻 - 梅州市文化馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
