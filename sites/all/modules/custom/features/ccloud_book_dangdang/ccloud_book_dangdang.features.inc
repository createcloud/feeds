<?php
/**
 * @file
 * ccloud_book_dangdang.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_book_dangdang_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_book_dangdang_node_info() {
  $items = array(
    'book_dangdang' => array(
      'name' => t('图书 - 当当'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'book_list_dangdang' => array(
      'name' => t('图书列表 - 当当'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
