<?php
/**
 * @file
 * ccloud_book_dangdang.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_book_dangdang_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'book_dangdang';
  $feeds_importer->config = array(
    'name' => 'Book - dangdang',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'div[contains(@class, \'show\')=\'true\']/div/div[@class=\'show_info\']/div/div[@class=\'book_messbox\']/div[last()-1]/div[@class=\'show_info_right\']',
          'xpathparser:1' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'authorintro\']/div[@class=\'descrip\']/textarea',
          'xpathparser:2' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'content\']/div[@class=\'descrip\']/textarea',
          'xpathparser:3' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'abstract\']/div[@class=\'descrip\']/textarea',
          'xpathparser:4' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'mediafeedback\']/div[@class=\'descrip\']/textarea',
          'xpathparser:5' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'catalog\']/div[@class=\'descrip\']/textarea',
          'xpathparser:6' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'extract\']/div[@class=\'descrip\']/textarea',
          'xpathparser:7' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@id=\'attach_image\']/div[@class=\'descrip\']/span',
          'xpathparser:8' => 'div[contains(@class, \'show\')=\'true\']/div/div[@class=\'show_info\']/div/div[@class=\'book_messbox\']/div[last()-3]/div[@class=\'show_info_right\']',
          'xpathparser:9' => 'div[contains(@class, \'show\')=\'true\']/div/div[@class=\'show_info\']/div/div[@class=\'book_messbox\']/div[last()-2]/div[@class=\'show_info_right\']',
          'xpathparser:10' => 'div[contains(@class, \'show\')=\'true\']/div/div[@class=\'show_pic\']/div[@id=\'detailPic\']/img/@src',
          'xpathparser:11' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-7]',
          'xpathparser:12' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-6]',
          'xpathparser:13' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-5]',
          'xpathparser:14' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-4]',
          'xpathparser:15' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-3]',
          'xpathparser:16' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-2]',
          'xpathparser:17' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()-1]',
          'xpathparser:18' => 'div[@id=\'product_tab\']/div[@id=\'detail_all\']/div[@class=\'section\']/ul/li[last()]',
        ),
        'rawXML' => array(
          'xpathparser:7' => 'xpathparser:7',
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:6' => 0,
          'xpathparser:8' => 0,
          'xpathparser:9' => 0,
          'xpathparser:10' => 0,
          'xpathparser:11' => 0,
          'xpathparser:12' => 0,
          'xpathparser:13' => 0,
          'xpathparser:14' => 0,
          'xpathparser:15' => 0,
          'xpathparser:16' => 0,
          'xpathparser:17' => 0,
          'xpathparser:18' => 0,
        ),
        'context' => '//div[@class=\'main\']',
        'exp' => array(
          'errors' => 0,
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
            'xpathparser:2' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
            'xpathparser:10' => 0,
            'xpathparser:11' => 0,
            'xpathparser:12' => 0,
            'xpathparser:13' => 0,
            'xpathparser:14' => 0,
            'xpathparser:15' => 0,
            'xpathparser:16' => 0,
            'xpathparser:17' => 0,
            'xpathparser:18' => 0,
          ),
        ),
        'allow_override' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:0',
            'target' => 'field_isbn',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:1',
            'target' => 'field_about_the_author',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:2',
            'target' => 'field_content_recommendation',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:3',
            'target' => 'field_editorial_recommendation',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:4',
            'target' => 'field_media_comments',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_toc',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_content_preview',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:7',
            'target' => 'field_attachment',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_publisher',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'xpathparser:9',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:10',
            'target' => 'field_image_large:uri',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:11',
            'target' => 'field_other_info_1',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'xpathparser:12',
            'target' => 'field_other_info_2',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'xpathparser:13',
            'target' => 'field_other_info_3',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'xpathparser:14',
            'target' => 'field_other_info_4',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'xpathparser:15',
            'target' => 'field_other_info_5',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'xpathparser:16',
            'target' => 'field_other_info_6',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'xpathparser:17',
            'target' => 'field_other_info_7',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'xpathparser:18',
            'target' => 'field_other_info_8',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'book_dangdang',
        'content_type' => 'book_dangdang',
      ),
    ),
    'content_type' => 'book_dangdang',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['book_dangdang'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'book_list_dangdang';
  $feeds_importer->config = array(
    'name' => 'Book list - dangdang',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'div[@class=\'name\']/a/@href',
          'xpathparser:1' => 'div[@class=\'name\']/a/@title',
          'xpathparser:2' => 'div[@class=\'publisher_info\']/a[last()]/@title',
          'xpathparser:3' => 'div[@class=\'pic\']/a/img/@src',
          'xpathparser:4' => 'div[@class=\'name\']/a/@href',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
        ),
        'context' => '//div[@class=\'bang_content\']/div[@class=\'bang_list_box\']/ul/li',
        'exp' => array(
          'errors' => 0,
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
            'xpathparser:2' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
          ),
        ),
        'allow_override' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:0',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'xpathparser:1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:2',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:3',
            'target' => 'field_image_thumbnail:uri',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:4',
            'target' => 'field_source_url',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'book_dangdang',
      ),
    ),
    'content_type' => 'book_list_dangdang',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['book_list_dangdang'] = $feeds_importer;

  return $export;
}
