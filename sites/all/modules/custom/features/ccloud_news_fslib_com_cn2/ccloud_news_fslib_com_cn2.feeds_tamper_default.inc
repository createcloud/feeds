<?php
/**
 * @file
 * ccloud_news_fslib_com_cn2.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_fslib_com_cn2_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_fslib_com_cn2-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_fslib_com_cn2';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_fslib_com_cn2-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_fslib_com_cn2-content-absolute_url';
  $feeds_tamper->importer = 'news_fslib_com_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_fslib_com_cn2-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_fslib_com_cn2-content-ccloud_remove_content';
  $feeds_tamper->importer = 'news_fslib_com_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'image' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
    ),
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_fslib_com_cn2-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_fslib_com_cn2-content-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_fslib_com_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '',
    'indexes' => '5',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_fslib_com_cn2-content-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_fslib_com_cn2-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_fslib_com_cn2';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_fslib_com_cn2-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_fslib_com_cn2-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_fslib_com_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 7;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_fslib_com_cn2-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_fslib_com_cn2-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_fslib_com_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 9;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_fslib_com_cn2-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_fslib_com_cn2-feed_source-ccloud_extract_tag_attribute_3';
  $feeds_tamper->importer = 'news_list_fslib_com_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'tr',
    'attribute' => 'onclick',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_fslib_com_cn2-feed_source-ccloud_extract_tag_attribute_3'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_fslib_com_cn2-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_list_fslib_com_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.aspx',
    'indexes' => '5',
  );
  $feeds_tamper->weight = 8;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_list_fslib_com_cn2-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_fslib_com_cn2-feed_source-replace2_link';
  $feeds_tamper->importer = 'news_list_fslib_com_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/window\\.open\\(\'([^\']+)\',\'_blank\',\'\'\\)/i',
    'replace' => '<a href="$1" >',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_list_fslib_com_cn2-feed_source-replace2_link'] = $feeds_tamper;

  return $export;
}
