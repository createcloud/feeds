<?php
/**
 * @file
 * ccloud_newspaper_common.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_newspaper_common_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'newspaper_generation-parent_nid-php';
  $feeds_tamper->importer = 'newspaper_generation';
  $feeds_tamper->source = 'parent:nid';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$nid = $field;
$wrapper = entity_metadata_wrapper(\'node\', $nid);
$list_type = $wrapper->field_newspaper_list_type->value();
$url_pattern = $wrapper->field_newspaper_url_pattern->value();

$time = time();
$year = gmdate(\'Y\', $time);
$month = gmdate(\'m\', $time);
$day = gmdate(\'d\', $time);
$tokens = array(
  \'{site}\' => $GLOBALS[\'base_url\'],
  \'{yyyy}\' => $year,
  \'{mm}\' => $month,
  \'{dd}\' => $day,
  \'{m}\' => (string) (int) $month,
  \'{d}\' => (string) (int) $day,
);

foreach ($tokens as $token => $value) {
  $url_pattern = str_replace($token, $value, $url_pattern);
}

$list_nids = entity_property_query(\'node\', \'field_newspaper_url\', $url_pattern);
if (empty($list_nids)) {
  $new_node = new stdClass();
  $new_node->type = $list_type;
  $new_node->title = $wrapper->title->value() . " - $year/$month/$day";
  $new_node->language = LANGUAGE_NONE;
  $new_node->is_new = TRUE;
  node_object_prepare($new_node);
  $new_node->uid = 1;
  $new_node->feeds[\'CreateCloudFeedsHTTPFetcher\'][\'source\'] = $url_pattern;
  node_save($new_node);
  $new_node_wrapper = entity_metadata_wrapper(\'node\', $new_node->nid);
  $new_node_wrapper->field_er_feed = $nid;
  $new_node_wrapper->field_newspaper_url = $url_pattern;
  $new_node_wrapper->field_date = mktime(0, 0, 0, $month, $day, $year);
  $new_node_wrapper->save();
}',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['newspaper_generation-parent_nid-php'] = $feeds_tamper;

  return $export;
}
