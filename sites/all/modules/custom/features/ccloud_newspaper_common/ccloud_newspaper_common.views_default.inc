<?php
/**
 * @file
 * ccloud_newspaper_common.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ccloud_newspaper_common_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'paper_news_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Paper news list';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_section_name',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: News list */
  $handler->display->display_options['relationships']['field_er_news_list_target_id']['id'] = 'field_er_news_list_target_id';
  $handler->display->display_options['relationships']['field_er_news_list_target_id']['table'] = 'field_data_field_er_news_list';
  $handler->display->display_options['relationships']['field_er_news_list_target_id']['field'] = 'field_er_news_list_target_id';
  $handler->display->display_options['relationships']['field_er_news_list_target_id']['ui_name'] = 'News list';
  $handler->display->display_options['relationships']['field_er_news_list_target_id']['label'] = 'News list';
  $handler->display->display_options['relationships']['field_er_news_list_target_id']['required'] = TRUE;
  /* Field: Content: Section name */
  $handler->display->display_options['fields']['field_section_name']['id'] = 'field_section_name';
  $handler->display->display_options['fields']['field_section_name']['table'] = 'field_data_field_section_name';
  $handler->display->display_options['fields']['field_section_name']['field'] = 'field_section_name';
  $handler->display->display_options['fields']['field_section_name']['label'] = '';
  $handler->display->display_options['fields']['field_section_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_section_name']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Section id (field_section_id) */
  $handler->display->display_options['sorts']['field_section_id_value']['id'] = 'field_section_id_value';
  $handler->display->display_options['sorts']['field_section_id_value']['table'] = 'field_data_field_section_id';
  $handler->display->display_options['sorts']['field_section_id_value']['field'] = 'field_section_id_value';
  /* Sort criterion: Content: Section name (field_section_name) */
  $handler->display->display_options['sorts']['field_section_name_value']['id'] = 'field_section_name_value';
  $handler->display->display_options['sorts']['field_section_name_value']['table'] = 'field_data_field_section_name';
  $handler->display->display_options['sorts']['field_section_name_value']['field'] = 'field_section_name_value';
  /* Sort criterion: Content: id (field_id) */
  $handler->display->display_options['sorts']['field_id_value']['id'] = 'field_id_value';
  $handler->display->display_options['sorts']['field_id_value']['table'] = 'field_data_field_id';
  $handler->display->display_options['sorts']['field_id_value']['field'] = 'field_id_value';
  /* Contextual filter: News list */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_er_news_list_target_id';
  $handler->display->display_options['arguments']['nid']['ui_name'] = 'News list';
  $handler->display->display_options['arguments']['nid']['exception']['title'] = '全部';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['argument_input'] = array(
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => '内容: Nid',
    ),
  );
  $export['paper_news_list'] = $view;

  return $export;
}
