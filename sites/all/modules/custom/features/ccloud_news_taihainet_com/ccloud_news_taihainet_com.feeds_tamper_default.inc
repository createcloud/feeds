<?php
/**
 * @file
 * ccloud_news_taihainet_com.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_taihainet_com_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_taihainet_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_taihainet_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_taihainet_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_taihainet_com-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_taihainet_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_taihainet_com-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_taihainet_com-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_taihainet_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_taihainet_com-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_taihainet_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-content-absolute_url';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_taihainet_com-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-content-ccloud_remove_content';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'parent:nid' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
      'image' => 0,
      'content_to_remove_2' => 1,
    ),
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_taihainet_com-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-content-ccloud_remove_content_2';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'parent:nid' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 0,
      'image' => 0,
      'content_to_remove_2' => 1,
    ),
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_taihainet_com-content-ccloud_remove_content_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-content-rewrite';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[parent:nid]
[content]',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'Rewrite';
  $export['news_taihainet_com-content-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-image-absolute_url';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_taihainet_com-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_taihainet_com-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_taihainet_com-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-next_page_url-absolute_url';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_taihainet_com-next_page_url-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-next_page_url-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_taihainet_com-next_page_url-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_taihainet_com-parent_nid-php';
  $feeds_tamper->importer = 'news_taihainet_com';
  $feeds_tamper->source = 'parent:nid';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$node = node_load($field);
$content = !empty($node->field_content[LANGUAGE_NONE][0][\'value\']) ? $node->field_content[LANGUAGE_NONE][0][\'value\'] : \'\';
return $content;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['news_taihainet_com-parent_nid-php'] = $feeds_tamper;

  return $export;
}
