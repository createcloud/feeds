<?php
/**
 * @file
 * ccloud_news_qq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_qq_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_qq_node_info() {
  $items = array(
    'news_list_qq' => array(
      'name' => t('新闻列表 - 腾讯'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'news_qq' => array(
      'name' => t('新闻 - 腾讯'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
