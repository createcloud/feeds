<?php
/**
 * @file
 * ccloud_paper_news_zjsb.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_paper_news_zjsb_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_1_zjsb-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_1_zjsb-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_list_1_zjsb-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.htm/',
    'indexes' => '7',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_1_zjsb-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-title-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '无标题
链接',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '无标题',
      1 => '链接',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_1_zjsb-title-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-title-php';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'preg_match("/[\\x{4e00}-\\x{9fa5}]/u", $field, $match);
return empty($match) ? \'\' : $field;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_1_zjsb-title-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_zjsb-title-required';
  $feeds_tamper->importer = 'paper_news_list_1_zjsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Required field';
  $export['paper_news_list_1_zjsb-title-required'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_zjsb-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_zjsb-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_zjsb-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_list_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_list_zjsb-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_zjsb-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_list_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.htm/./',
    'indexes' => '7,8',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_zjsb-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_zjsb-feed_source-ccloud_remove_url_segment_2';
  $feeds_tamper->importer = 'paper_news_list_zjsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.htm/',
    'indexes' => '7',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_zjsb-feed_source-ccloud_remove_url_segment_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_zjsb-title-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_zjsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '第F01版：封面',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '第F01版：封面',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_zjsb-title-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_zjsb-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-content-absolute_url';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_zjsb-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-content-ccloud_remove_content';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'image' => 0,
      'author' => 0,
      'source' => 0,
      'publication_date' => 0,
      'content_to_remove' => 1,
      'content_to_remove_2' => 1,
    ),
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['paper_news_zjsb-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-content-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '../../../',
    'indexes' => '4,5,6,7,8,9,10',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_zjsb-content-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-content-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/<\\/?(table|td|tr|tbody)[^>]{0,}>/i',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_zjsb-content-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-image-absolute_url';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_zjsb-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 7;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_zjsb-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-image-ccloud_fetch_specified_items';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_fetch_specified_items';
  $feeds_tamper->settings = array(
    'indexes' => '0',
    'return_string' => 1,
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Fetch specified item(s)';
  $export['paper_news_zjsb-image-ccloud_fetch_specified_items'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-image-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '../../../',
    'indexes' => '4,5,6,7,8,9,10',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_zjsb-image-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_zjsb-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'paper_news_zjsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 8;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['paper_news_zjsb-image-ccloud_restrict_image'] = $feeds_tamper;

  return $export;
}
