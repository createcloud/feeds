<?php
/**
 * @file
 * ccloud_news_news_dlxww_com.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_news_dlxww_com_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_news_dlxww_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_news_dlxww_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_news_dlxww_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_news_dlxww_com-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_news_dlxww_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_news_dlxww_com-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_news_dlxww_com-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_news_dlxww_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_news_dlxww_com-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_news_dlxww_com-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_list_news_dlxww_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => 'node',
    'indexes' => '4',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_list_news_dlxww_com-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-author-find_replace_regex';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'author';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/([\\s|\\S]+)\\d{4}-\\d{1,2}-\\d{1,2}[\\s|\\S]+/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_news_dlxww_com-author-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_news_dlxww_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-content-absolute_url';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_news_dlxww_com-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-content-ccloud_remove_content';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
    ),
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_news_dlxww_com-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-content-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '../..',
    'indexes' => '5,6,7,8,9',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_news_dlxww_com-content-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-content-ccloud_remove_url_segment_a_href';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => 'htm',
    'indexes' => '7',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_news_dlxww_com-content-ccloud_remove_url_segment_a_href'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-image-absolute_url';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_news_dlxww_com-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_news_dlxww_com-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-image-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '../..',
    'indexes' => '5,6,7,8,9',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_news_dlxww_com-image-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_news_dlxww_com-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_dlxww_com-publication_date-find_replace_regex';
  $feeds_tamper->importer = 'news_news_dlxww_com';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/[\\s|\\S]+(\\d{4}-\\d{1,2}-\\d{1,2}[\\s|\\S]+)/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_news_dlxww_com-publication_date-find_replace_regex'] = $feeds_tamper;

  return $export;
}
