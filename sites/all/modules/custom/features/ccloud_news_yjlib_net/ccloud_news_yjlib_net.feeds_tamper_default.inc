<?php
/**
 * @file
 * ccloud_news_yjlib_net.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_yjlib_net_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_yjlib_net-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_yjlib_net';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_yjlib_net-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_yjlib_net-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_yjlib_net';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_yjlib_net-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_yjlib_net-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_yjlib_net';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_yjlib_net-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_yjlib_net-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_yjlib_net';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_yjlib_net-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_yjlib_net-content-absolute_url';
  $feeds_tamper->importer = 'news_yjlib_net';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_yjlib_net-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_yjlib_net-image-absolute_url';
  $feeds_tamper->importer = 'news_yjlib_net';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_yjlib_net-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_yjlib_net-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_yjlib_net';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_yjlib_net-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_yjlib_net-image-ccloud_fetch_specified_items';
  $feeds_tamper->importer = 'news_yjlib_net';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_fetch_specified_items';
  $feeds_tamper->settings = array(
    'indexes' => '0',
    'return_string' => 1,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Fetch specified item(s)';
  $export['news_yjlib_net-image-ccloud_fetch_specified_items'] = $feeds_tamper;

  return $export;
}
