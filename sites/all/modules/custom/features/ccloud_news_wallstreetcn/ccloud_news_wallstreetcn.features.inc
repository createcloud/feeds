<?php
/**
 * @file
 * ccloud_news_wallstreetcn.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_wallstreetcn_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_wallstreetcn_node_info() {
  $items = array(
    'item_wallstreetcn' => array(
      'name' => t('ITEM - 华尔街见闻'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rss_wallstreetcn' => array(
      'name' => t('RSS - 华尔街见闻'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
