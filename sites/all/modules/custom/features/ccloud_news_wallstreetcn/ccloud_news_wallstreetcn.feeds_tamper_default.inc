<?php
/**
 * @file
 * ccloud_news_wallstreetcn.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_wallstreetcn_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'item_wallstreetcn-content-strip_tags';
  $feeds_tamper->importer = 'item_wallstreetcn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<p><a><img><em><strong><h3><h4><h5>',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Strip tags';
  $export['item_wallstreetcn-content-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_wallstreetcn-cover_image-trim';
  $feeds_tamper->importer = 'rss_wallstreetcn';
  $feeds_tamper->source = 'cover_image';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '!index-news-cover',
    'side' => 'rtrim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['rss_wallstreetcn-cover_image-trim'] = $feeds_tamper;

  return $export;
}
