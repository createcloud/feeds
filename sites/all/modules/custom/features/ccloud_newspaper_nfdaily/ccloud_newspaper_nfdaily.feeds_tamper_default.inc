<?php
/**
 * @file
 * ccloud_newspaper_nfdaily.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_newspaper_nfdaily_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nfdaily-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_nfdaily';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_nfdaily-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nfdaily-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_nfdaily';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_nfdaily-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nfdaily-feed_source-php';
  $feeds_tamper->importer = 'paper_news_list_nfdaily';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$a_match = array();
$href_match = array();
$href_url = \'\';
if (preg_match("/<a[^>]+>/i", $field, $a_match)) {
  if (preg_match(\'/(href)="([^"]*)"/i\', $a_match[0], $href_match))
    $href_url = $href_match[2];
}

$href_url_array = explode(\'://\', $href_url);
if (!empty($href_url_array[1])) {
  $scheme = $href_url_array[0] . \'://\';
  $href_url = $href_url_array[1];
  $href_url_array = explode(\'/\', $href_url);
  unset($href_url_array[count($href_url_array) - 2]);
  $href_url = $scheme . implode(\'/\', $href_url_array);
}
return $href_url;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_nfdaily-feed_source-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_nfdaily-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-content-absolute_url';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_nfdaily-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-content-php';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match_all(\'/(src)="([^"]*)"/i\', $field, $str_match)) {
  $urls = $str_match[2];
  foreach ($urls as $url) {
    $url_parts = explode(\'/\', $url);
    unset($url_parts[4]);
    unset($url_parts[5]);
    unset($url_parts[6]);
    unset($url_parts[7]);
    unset($url_parts[8]);
    unset($url_parts[9]);
    unset($url_parts[10]);
    $field = str_replace($url, implode(\'/\', $url_parts), $field);
  }
}
return $field;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_nfdaily-content-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-image-absolute_url';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_nfdaily-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['paper_news_nfdaily-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-image-php';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$img_match = array();
$src_match = array();
$img_url = \'\';
if (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  if (preg_match(\'/(src)="([^"]*)"/i\', $img_match[0], $src_match))
    $img_url = $src_match[2];
    $img_url_array = explode(\'/\', $img_url);
    unset($img_url_array[4]);
    unset($img_url_array[5]);
    unset($img_url_array[6]);
    unset($img_url_array[7]);
    unset($img_url_array[8]);
    unset($img_url_array[9]);
    unset($img_url_array[10]);
    $img_url = implode(\'/\', $img_url_array);
}
return $img_url;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_nfdaily-image-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nfdaily-publication_date-trim';
  $feeds_tamper->importer = 'paper_news_nfdaily';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_nfdaily-publication_date-trim'] = $feeds_tamper;

  return $export;
}
