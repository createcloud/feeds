<?php
/**
 * @file
 * ccloud_news_hynews_org.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_news_hynews_org_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '河源新闻网 - 社会热点',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1442975343,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '0d99d515-e296-45a7-86e8-5fca29698686',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '新闻',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '河源新闻网',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.hynews.org/news/sh/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_hynews_org',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_area' => array(),
  'field_feed_info_gdnews_type' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-09-23 02:29:03 +0000',
  'user_uuid' => 'c70ba0ea-403d-4a48-bbcb-793d9302d8ab',
);
  $nodes[] = array(
  'title' => '河源新闻网 - 时政要闻',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1442975297,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '94508ae3-892c-407e-a822-afb0911b94ee',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '新闻',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '河源新闻网',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.hynews.org/news/sz/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_hynews_org',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_area' => array(),
  'field_feed_info_gdnews_type' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-09-23 02:28:17 +0000',
  'user_uuid' => 'c70ba0ea-403d-4a48-bbcb-793d9302d8ab',
);
  return $nodes;
}
