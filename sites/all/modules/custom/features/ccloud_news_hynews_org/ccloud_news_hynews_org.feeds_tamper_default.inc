<?php
/**
 * @file
 * ccloud_news_hynews_org.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_hynews_org_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_hynews_org-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_hynews_org';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_hynews_org-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_hynews_org-content-ccloud_remove_content';
  $feeds_tamper->importer = 'news_hynews_org';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
      'image' => 0,
    ),
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_hynews_org-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_hynews_org-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_hynews_org';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_hynews_org-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_hynews_org-publication_date-find_replace_regex';
  $feeds_tamper->importer = 'news_hynews_org';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{2}:\\d{2})[\\s|\\S]+/i',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_hynews_org-publication_date-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_hynews_org-source-find_replace_regex';
  $feeds_tamper->importer = 'news_hynews_org';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/[\\s|\\S]+来源：([\\s|\\S]+)点击：/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_hynews_org-source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_hynews_org-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_hynews_org';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_hynews_org-blank_source_1-default_value'] = $feeds_tamper;

  return $export;
}
