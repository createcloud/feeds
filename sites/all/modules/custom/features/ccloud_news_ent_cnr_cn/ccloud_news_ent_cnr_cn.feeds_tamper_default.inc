<?php
/**
 * @file
 * ccloud_news_ent_cnr_cn.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_ent_cnr_cn_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_ent_cnr_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-content-absolute_url';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_ent_cnr_cn-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-content-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => './',
    'indexes' => '5,6',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_ent_cnr_cn-content-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-content-rewrite';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[parent:nid]
[content]',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Rewrite';
  $export['news_ent_cnr_cn-content-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-image-absolute_url';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_ent_cnr_cn-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 7;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_ent_cnr_cn-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-image-ccloud_fetch_specified_items';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_fetch_specified_items';
  $feeds_tamper->settings = array(
    'indexes' => '0',
    'return_string' => 1,
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Fetch specified item(s)';
  $export['news_ent_cnr_cn-image-ccloud_fetch_specified_items'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-image-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => './',
    'indexes' => '5,6',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_ent_cnr_cn-image-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 8;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_ent_cnr_cn-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-next_page_url-absolute_url';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_ent_cnr_cn-next_page_url-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-next_page_url-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_ent_cnr_cn-next_page_url-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-next_page_url-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => 'html/',
    'indexes' => '5',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_ent_cnr_cn-next_page_url-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-parent_nid-php';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'parent:nid';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$node = node_load($field);
$content = !empty($node->field_content[LANGUAGE_NONE][0][\'value\']) ? $node->field_content[LANGUAGE_NONE][0][\'value\'] : \'\';
return $content;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['news_ent_cnr_cn-parent_nid-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-publication_date-find_replace_regex';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/([\\s|\\S]+)来源：([\\s|\\S]+)/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_ent_cnr_cn-publication_date-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_ent_cnr_cn-source-find_replace_regex';
  $feeds_tamper->importer = 'news_ent_cnr_cn';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/([\\s|\\S]+)来源：([\\s|\\S]+)/',
    'replace' => '$2',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_ent_cnr_cn-source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_ent_cnr_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_ent_cnr_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_ent_cnr_cn-blank_source_1-default_value'] = $feeds_tamper;

  return $export;
}
