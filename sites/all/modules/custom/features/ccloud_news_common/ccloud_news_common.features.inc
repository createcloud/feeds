<?php
/**
 * @file
 * ccloud_news_common.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ccloud_news_common_node_info() {
  $items = array(
    'news' => array(
      'name' => t('新闻'),
      'base' => 'node_content',
      'description' => t('This content type is being used for automatically aggregated content from feeds.'),
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
