<?php
/**
 * @file
 * ccloud_culture_jguo_cn.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_culture_jguo_cn_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_culture_jguo_cn_node_info() {
  $items = array(
    'culture_jguo_cn' => array(
      'name' => t('文化 - 家国'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'culture_list_jguo_cn' => array(
      'name' => t('文化列表 - 家国'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
