<?php
/**
 * @file
 * ccloud_culture_jguo_cn.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_culture_jguo_cn_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'culture_jguo_cn';
  $feeds_importer->config = array(
    'name' => '文化 - 家国',
    'description' => '家国',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '//div[contains(@class,\'pleft\')]/div[@class=\'viewbox\']//ul[@class=\'pagelist\']/li[contains(.,\'下一页\')]/a[not(@href=\'#\')]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => 0,
        'sources' => array(
          'content' => array(
            'name' => 'Content',
            'value' => 'div[@id=\'content\']',
            'raw' => 1,
            'debug' => 0,
            'weight' => '1',
          ),
          'author' => array(
            'name' => 'Author',
            'value' => 'div[@class=\'info\']/small[contains(.,\'来源\')]/following-sibling::text()[1]',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'publication_date' => array(
            'name' => 'Publication date',
            'value' => 'div[@class=\'info\']/small[contains(.,\'时间\')]/following-sibling::text()[1]',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
          'source' => array(
            'name' => 'Source',
            'value' => '',
            'raw' => 1,
            'debug' => 0,
            'weight' => '4',
          ),
          'content_to_remove' => array(
            'name' => 'Content to remove',
            'value' => '',
            'raw' => 1,
            'debug' => 0,
            'weight' => '5',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => 'div[@id=\'content\']//img[1]/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '6',
          ),
          'next_page_url' => array(
            'name' => 'Next page url',
            'value' => '//ul[@class=\'pagelist\']/li[contains(.,\'下一页\')]/a[not(@href=\'#\')]',
            'raw' => 1,
            'debug' => 0,
            'weight' => '7',
          ),
        ),
        'context' => array(
          'value' => '//div[contains(@class,\'pleft\')]/div[@class=\'viewbox\']',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'parent:nid',
            'target' => 'Temporary target 2',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'publication_date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'next_page_url',
            'target' => 'field_next_page_url',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'content_to_remove',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'next_page_url',
            'target' => 'field_next_page_url',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'culture_jguo_cn',
      ),
    ),
    'content_type' => 'culture_jguo_cn',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['culture_jguo_cn'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'culture_list_jguo_cn';
  $feeds_importer->config = array(
    'name' => '文化列表 - 家国',
    'description' => '家国',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => '.',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => '@href',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'context' => array(
          'value' => '//div[@class=\'listbox\']/ul/li/a',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'format' => 'filtered_html',
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'culture_jguo_cn',
      ),
    ),
    'content_type' => 'culture_list_jguo_cn',
    'update' => 0,
    'import_period' => '604800',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['culture_list_jguo_cn'] = $feeds_importer;

  return $export;
}
