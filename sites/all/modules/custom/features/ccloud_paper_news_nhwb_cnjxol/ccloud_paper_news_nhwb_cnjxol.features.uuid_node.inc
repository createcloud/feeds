<?php
/**
 * @file
 * ccloud_paper_news_nhwb_cnjxol.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_paper_news_nhwb_cnjxol_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '嘉兴日报',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1441769048,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'f40c9789-a27e-4af6-901d-56d153458bd3',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '全国',
        'format' => NULL,
      ),
      1 => array(
        'value' => '报纸',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '嘉兴日报',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '报纸',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://nhwb.cnjxol.com/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'newspaper',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_list_type' => array(
    'und' => array(
      0 => array(
        'value' => 'paper_news_list_nhwb_cnjxol',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_url_pattern' => array(
    'und' => array(
      0 => array(
        'value' => 'http://nhwb.cnjxol.com/html/{yyyy}-{mm}/{dd}/node_33.htm',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_gdnews_area' => array(),
  'field_feed_info_gdnews_type' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-09-09 03:24:08 +0000',
  'user_uuid' => 'c70ba0ea-403d-4a48-bbcb-793d9302d8ab',
);
  return $nodes;
}
