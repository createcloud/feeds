<?php
/**
 * @file
 * ccloud_paper_news_nhwb_cnjxol.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_paper_news_nhwb_cnjxol_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_paper_news_nhwb_cnjxol_node_info() {
  $items = array(
    'paper_news_list_1_nhwb_cnjxol' => array(
      'name' => t('报纸新闻列表一 - 嘉兴日报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'paper_news_list_nhwb_cnjxol' => array(
      'name' => t('报纸新闻列表 - 嘉兴日报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'paper_news_nhwb_cnjxol' => array(
      'name' => t('报纸新闻 - 嘉兴日报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
