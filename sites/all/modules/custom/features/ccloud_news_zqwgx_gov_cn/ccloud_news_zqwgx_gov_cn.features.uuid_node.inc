<?php
/**
 * @file
 * ccloud_news_zqwgx_gov_cn.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_news_zqwgx_gov_cn_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '肇庆文化广电 - 文化新闻',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440468873,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'af1b41c0-57bd-43f4-abff-05200d68ea2c',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '广东文化',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.zqwgx.gov.cn/whxw/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '肇庆市文化广电新闻出版局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_zqwgx_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_gdnews_area' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-25 10:14:33 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '肇庆文化广电 - 通知公告',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440468833,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'ec5c9e9d-e3bf-4f1f-a382-2521956f214a',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '广东文化',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.zqwgx.gov.cn/tzgg/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '肇庆市文化广电新闻出版局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_zqwgx_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_type' => array(
    'und' => array(
      0 => array(
        'value' => '公告',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_gdnews_area' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-25 10:13:53 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  return $nodes;
}
