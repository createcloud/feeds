<?php
/**
 * @file
 * ccloud_culture_wenhuaw_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_culture_wenhuaw_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_culture_wenhuaw_com_node_info() {
  $items = array(
    'culture_list_wenhuaw_com' => array(
      'name' => t('文化列表 - 文化生活网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'culture_wenhuaw_com' => array(
      'name' => t('文化 - 文化生活网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
