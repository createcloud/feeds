<?php
/**
 * @file
 * ccloud_culture_wenhuaw_com.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_culture_wenhuaw_com_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_wenhuaw_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'culture_list_wenhuaw_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['culture_list_wenhuaw_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_wenhuaw_com-feed_source-absolute_url';
  $feeds_tamper->importer = 'culture_list_wenhuaw_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['culture_list_wenhuaw_com-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_wenhuaw_com-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'culture_list_wenhuaw_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['culture_list_wenhuaw_com-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_wenhuaw_com-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'culture_list_wenhuaw_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => './',
    'indexes' => '3,4',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['culture_list_wenhuaw_com-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-author-find_replace_regex';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'author';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/[\\s|\\S]+来源：([\\s|\\S]+)浏览：[\\s|\\S]+/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['culture_wenhuaw_com-author-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['culture_wenhuaw_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-content-ccloud_remove_content';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
    ),
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['culture_wenhuaw_com-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-content-rewrite';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[parent:nid]
[content]',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Rewrite';
  $export['culture_wenhuaw_com-content-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['culture_wenhuaw_com-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-next_page_url-absolute_url';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 17;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['culture_wenhuaw_com-next_page_url-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-next_page_url-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 18;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['culture_wenhuaw_com-next_page_url-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-next_page_url-find_replace_regex';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/\\?id=\\d+&(thisPage=\\d+)/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 20;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['culture_wenhuaw_com-next_page_url-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-next_page_url-find_replace_regex_2';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'next_page_url';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(thisPage=\\d+)/',
    'replace' => '&$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 21;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['culture_wenhuaw_com-next_page_url-find_replace_regex_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-parent_nid-php';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'parent:nid';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$node = node_load($field);
$content = !empty($node->field_content[LANGUAGE_NONE][0][\'value\']) ? $node->field_content[LANGUAGE_NONE][0][\'value\'] : \'\';
return $content;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['culture_wenhuaw_com-parent_nid-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-publication_date-find_replace_regex';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/([\\s|\\S]+)来源：[\\s|\\S]+浏览：[\\s|\\S]+/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['culture_wenhuaw_com-publication_date-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_wenhuaw_com-source-find_replace_regex';
  $feeds_tamper->importer = 'culture_wenhuaw_com';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/[\\s|\\S]+来源：([\\s|\\S]+)浏览：[\\s|\\S]+/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['culture_wenhuaw_com-source-find_replace_regex'] = $feeds_tamper;

  return $export;
}
