<?php
/**
 * @file
 * ccloud_paper_news_hxdsb.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_paper_news_hxdsb_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_hxdsb-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-content-absolute_url';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_hxdsb-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-image-absolute_url';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_hxdsb-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_hxdsb-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-image-ccloud_fetch_specified_items';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_fetch_specified_items';
  $feeds_tamper->settings = array(
    'indexes' => '0',
    'return_string' => 1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Fetch specified item(s)';
  $export['paper_news_hxdsb-image-ccloud_fetch_specified_items'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['paper_news_hxdsb-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-publication_date-find_replace';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => '日期：[',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace';
  $export['paper_news_hxdsb-publication_date-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_hxdsb-publication_date-find_replace_2';
  $feeds_tamper->importer = 'paper_news_hxdsb';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => ']',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace';
  $export['paper_news_hxdsb-publication_date-find_replace_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_1_hxdsb-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_1_hxdsb-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_list_1_hxdsb-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.shtml/',
    'indexes' => '6',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_1_hxdsb-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-feed_source-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/onclick="gotourl\\(\'(\\w+\\.shtml)\',0\\)"/',
    'replace' => 'href="$1"',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_list_1_hxdsb-feed_source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-feed_source-find_replace_regex_2';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(<\\/?)dd/i',
    'replace' => '$1a',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_list_1_hxdsb-feed_source-find_replace_regex_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-title-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '广告',
    'word_boundaries' => 1,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '/\\b广告\\b/ui',
    ),
    'regex' => TRUE,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_1_hxdsb-title-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_hxdsb-title-trim';
  $feeds_tamper->importer = 'paper_news_list_1_hxdsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_list_1_hxdsb-title-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_hxdsb-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_list_hxdsb-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.shtml/',
    'indexes' => '6',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_hxdsb-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-feed_source-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/onclick="gotourl\\(\'(v\\w\\d{2}\\.shtml)\',0\\)"/i',
    'replace' => 'href="$1"',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_list_hxdsb-feed_source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-feed_source-find_replace_regex_2';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(<\\/?)li/',
    'replace' => '$1a',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_list_hxdsb-feed_source-find_replace_regex_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-feed_source-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => 'vb
vc
vd
ve
vf',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => 'vb',
      1 => 'vc',
      2 => 'vd',
      3 => 've',
      4 => 'vf',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_hxdsb-feed_source-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_hxdsb-title-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_hxdsb';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '要闻',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '要闻',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_hxdsb-title-keyword_filter'] = $feeds_tamper;

  return $export;
}
