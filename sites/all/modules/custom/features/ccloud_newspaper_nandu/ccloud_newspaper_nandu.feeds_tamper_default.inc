<?php
/**
 * @file
 * ccloud_newspaper_nandu.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_newspaper_nandu_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_nandu-xpathparser_0-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_1_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_1_nandu-xpathparser_0-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_nandu-xpathparser_0-php';
  $feeds_tamper->importer = 'paper_news_list_1_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match(\'/(href)="([^"]*)"/i\', $field, $str_match)) {
  $url = $str_match[2];
}
if (strpos($url, \'./\')) {
  $url = str_replace(\'./\', \'\', $url);
}
$url_parts = explode(\'/\', $url);
unset($url_parts[count($url_parts) - 2]);
$url = implode(\'/\', $url_parts);
return $url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_1_nandu-xpathparser_0-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_nandu-xpathparser_1-trim';
  $feeds_tamper->importer = 'paper_news_list_1_nandu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_list_1_nandu-xpathparser_1-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_nandu-xpathparser_2-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_1_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_1_nandu-xpathparser_2-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_nandu-xpathparser_2-php';
  $feeds_tamper->importer = 'paper_news_list_1_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match(\'/(href)="([^"]*)"/i\', $field, $str_match)) {
  $url = $str_match[2];
}
if (strpos($url, \'./\')) {
  $url = str_replace(\'./\', \'\', $url);
}
$url_parts = explode(\'/\', $url);
unset($url_parts[count($url_parts) - 2]);
$url = implode(\'/\', $url_parts);
return $url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_1_nandu-xpathparser_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_2_nandu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_0-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_2_nandu-xpathparser_0-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_0-find_replace';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'area',
    'replace' => 'a',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace';
  $export['paper_news_list_2_nandu-xpathparser_0-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_0-php';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match(\'/(href)="([^"]*)"/i\', $field, $str_match)) {
  $url = $str_match[2];
}
$url_parts = explode(\'/\', $url);
unset($url_parts[count($url_parts) - 2]);
$url = implode(\'/\', $url_parts);
$url_parts = explode(\'?\', $url);
$url = $url_parts[0];
return $url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_2_nandu-xpathparser_0-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '＊标题待更新＊',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_2_nandu-xpathparser_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_2-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_2_nandu-xpathparser_2-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_2-find_replace';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'area',
    'replace' => 'a',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace';
  $export['paper_news_list_2_nandu-xpathparser_2-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_2_nandu-xpathparser_2-php';
  $feeds_tamper->importer = 'paper_news_list_2_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match(\'/(href)="([^"]*)"/i\', $field, $str_match)) {
  $url = $str_match[2];
}
$url_parts = explode(\'/\', $url);
unset($url_parts[count($url_parts) - 2]);
$url = implode(\'/\', $url_parts);
$url_parts = explode(\'?\', $url);
$url = $url_parts[0];
return $url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_2_nandu-xpathparser_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_3_nandu-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_3_nandu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_3_nandu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_3_nandu-xpathparser_1-trim';
  $feeds_tamper->importer = 'paper_news_list_3_nandu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_list_3_nandu-xpathparser_1-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nandu-xpathparser_0-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_nandu-xpathparser_0-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nandu-xpathparser_0-php';
  $feeds_tamper->importer = 'paper_news_list_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match(\'/(href)="([^"]*)"/i\', $field, $str_match)) {
  $url = $str_match[2];
}

$url_parts = explode(\'/\', $url);

if (strpos($url, \'../../../../\')) {
  $url = str_replace(\'../../../../\', \'\', $url);
  $url_parts = explode(\'/\', $url);
  unset($url_parts[4]);
  unset($url_parts[5]);
  unset($url_parts[6]);
  unset($url_parts[7]);
  unset($url_parts[8]);
}
elseif (strpos($url, \'./\')) {
  $url = str_replace(\'./\', \'\', $url);
  $url_parts = explode(\'/\', $url);
  unset($url_parts[count($url_parts) - 1]);
}

$url = implode(\'/\', $url_parts);
return $url;',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_nandu-xpathparser_0-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nandu-xpathparser_1-trim';
  $feeds_tamper->importer = 'paper_news_list_nandu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_list_nandu-xpathparser_1-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nandu-xpathparser_2-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_nandu-xpathparser_2-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_nandu-xpathparser_2-php';
  $feeds_tamper->importer = 'paper_news_list_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match(\'/(href)="([^"]*)"/i\', $field, $str_match)) {
  $url = $str_match[2];
}

$url_parts = explode(\'/\', $url);

if (strpos($url, \'../../../../\')) {
  $url = str_replace(\'../../../../\', \'\', $url);
  $url_parts = explode(\'/\', $url);
  unset($url_parts[4]);
  unset($url_parts[5]);
  unset($url_parts[6]);
  unset($url_parts[7]);
  unset($url_parts[8]);
}
elseif (strpos($url, \'./\')) {
  $url = str_replace(\'./\', \'\', $url);
  $url_parts = explode(\'/\', $url);
  unset($url_parts[count($url_parts) - 1]);
}

$url = implode(\'/\', $url_parts);
return $url;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_list_nandu-xpathparser_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_nandu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-blank_source_2-ccloud_restrict_image';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['paper_news_nandu-blank_source_2-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-blank_source_2-php';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$img_match = array();
$src_match = array();
$img_url = \'\';
if (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  if (preg_match(\'/(src)="([^"]*)"/i\', $img_match[0], $src_match))
    $img_url = ($src_match[2]);
}

return $img_url;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_nandu-blank_source_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-blank_source_2-rewrite';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[xpathparser:0]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['paper_news_nandu-blank_source_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_0-absolute_url';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_nandu-xpathparser_0-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_0-php';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if (preg_match_all(\'/(src)="([^"]*)"/i\', $field, $str_match)) {
  $urls = $str_match[2];
  foreach ($urls as $url) {
    $url_parts = explode(\'/\', $url);
    unset($url_parts[5]);
    unset($url_parts[6]);
    unset($url_parts[7]);
    unset($url_parts[8]);
    unset($url_parts[9]);
    unset($url_parts[10]);
    unset($url_parts[11]);
    $field = str_replace($url, implode(\'/\', $url_parts), $field);
  }
}
return $field;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['paper_news_nandu-xpathparser_0-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_1-find_replace';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => '作者：',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace';
  $export['paper_news_nandu-xpathparser_1-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_1-trim';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_nandu-xpathparser_1-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_2-trim';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_nandu-xpathparser_2-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_3-find_replace';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:3';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => '来源：',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace';
  $export['paper_news_nandu-xpathparser_3-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_3-trim';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:3';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_nandu-xpathparser_3-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_4-keyword_filter';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:4';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '广告
彩票
主页
新闻串串烧',
    'word_boundaries' => 0,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '广告',
      1 => '彩票',
      2 => '主页',
      3 => '新闻串串烧',
    ),
    'regex' => FALSE,
    'func' => 'mb_stripos',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_nandu-xpathparser_4-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_4-required';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:4';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Required field';
  $export['paper_news_nandu-xpathparser_4-required'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_nandu-xpathparser_4-trim';
  $feeds_tamper->importer = 'paper_news_nandu';
  $feeds_tamper->source = 'xpathparser:4';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['paper_news_nandu-xpathparser_4-trim'] = $feeds_tamper;

  return $export;
}
