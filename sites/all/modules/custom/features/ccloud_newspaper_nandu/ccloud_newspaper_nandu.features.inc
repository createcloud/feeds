<?php
/**
 * @file
 * ccloud_newspaper_nandu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_newspaper_nandu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_newspaper_nandu_node_info() {
  $items = array(
    'paper_news_list_1_nandu' => array(
      'name' => t('报纸新闻列表一 - 南方都市报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'paper_news_list_2_nandu' => array(
      'name' => t('报纸新闻列表二 - 南方都市报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'paper_news_list_nandu' => array(
      'name' => t('报纸新闻列表 - 南方都市报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'paper_news_nandu' => array(
      'name' => t('报纸新闻 - 南方都市报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
