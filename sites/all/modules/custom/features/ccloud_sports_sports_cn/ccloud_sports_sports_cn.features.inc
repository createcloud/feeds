<?php
/**
 * @file
 * ccloud_sports_sports_cn.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_sports_sports_cn_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_sports_sports_cn_node_info() {
  $items = array(
    'sports_list_sports_cn' => array(
      'name' => t('体育列表 - 华奥星空'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sports_sports_cn' => array(
      'name' => t('体育 - 华奥星空'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
