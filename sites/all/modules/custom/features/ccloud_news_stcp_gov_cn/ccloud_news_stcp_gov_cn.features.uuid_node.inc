<?php
/**
 * @file
 * ccloud_news_stcp_gov_cn.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_news_stcp_gov_cn_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '汕头市文化广电新闻出版局 - 展演信息 ',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1448898632,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '7cfce7cd-e903-4106-a2fc-c8aec7094d05',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '广东文化',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.stcp.gov.cn/list.aspx?cid=81',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '汕头市文化广电新闻出版局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_stcp_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_type' => array(
    'und' => array(
      0 => array(
        'value' => '演艺',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_gdnews_area' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-11-30 23:50:32 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '汕头市文化广电新闻出版局 - 资讯',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1448898578,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '7d024b77-1492-44c8-bef8-eab5f1802b16',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '广东文化',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.stcp.gov.cn/list.aspx?cid=10',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '汕头市文化广电新闻出版局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_stcp_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_gdnews_area' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-11-30 23:49:38 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  return $nodes;
}
