<?php
/**
 * @file
 * ccloud_video_zslib_lecture_2.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_video_zslib_lecture_2_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_list_zslib_lecture_2';
  $feeds_importer->config = array(
    'name' => '视频列表 - 中山图书馆讲座（新）',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => 'h2',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '2',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => 'img',
            'raw' => 1,
            'debug' => 0,
            'weight' => '3',
          ),
          'speaker' => array(
            'name' => 'Speaker',
            'value' => 'p[@id=\'auth\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '4',
          ),
          'time' => array(
            'name' => 'Time',
            'value' => 'p[@id=\'time\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '5',
          ),
          'order' => array(
            'name' => 'Order',
            'value' => './../div[@id=\'zsztid\' or @id=\'yhttid\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '6',
          ),
        ),
        'context' => array(
          'value' => '//div[@class=\'tabcontent\']//li/a',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'format' => 'filtered_html',
          ),
          5 => array(
            'source' => 'speaker',
            'target' => 'field_speaker',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'time',
            'target' => 'field_date_time',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'order',
            'target' => 'field_order',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'video_zslib_lecture_2',
      ),
    ),
    'content_type' => 'video_list_zslib_lecture_2',
    'update' => 0,
    'import_period' => '86400',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_list_zslib_lecture_2'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_zslib_lecture_2';
  $feeds_importer->config = array(
    'name' => '视频 - 中山图书馆讲座（新）',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//video',
        ),
        'sources' => array(
          'video_url' => array(
            'name' => 'Video URL',
            'value' => 'source/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'video_url',
            'target' => 'field_video_url',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'video_zslib_lecture_2',
      ),
    ),
    'content_type' => 'video_zslib_lecture_2',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_zslib_lecture_2'] = $feeds_importer;

  return $export;
}
