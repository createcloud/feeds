<?php
/**
 * @file
 * ccloud_newspaper_ycwb.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_newspaper_ycwb_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_newspaper_ycwb_node_info() {
  $items = array(
    'paper_news_list_ycwb' => array(
      'name' => t('报纸新闻列表 - 羊城晚报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'paper_news_ycwb' => array(
      'name' => t('报纸新闻 - 羊城晚报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
