<?php
/**
 * @file
 * ccloud_news_guokr.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_news_guokr_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'post_guokr';
  $feeds_importer->config = array(
    'name' => '帖子 - 果壳',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//div[@class="content"]',
        ),
        'sources' => array(
          'author' => array(
            'name' => '作者',
            'value' => '//div[@class=\'content-th-info\']/a/@title',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'date' => array(
            'name' => '时间',
            'value' => '//div[@class=\'content-th-info\']/meta/@content',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'content' => array(
            'name' => '正文',
            'value' => '//div[@id=\'articleContent\']',
            'raw' => 1,
            'debug' => 0,
            'weight' => '4',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'post_guokr',
      ),
    ),
    'content_type' => 'post_guokr',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['post_guokr'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'rss_guokr';
  $feeds_importer->config = array(
    'name' => 'RSS - 果壳',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
        'one_time_source' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => '标题',
            'value' => 'h3/a',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'link' => array(
            'name' => '链接',
            'value' => 'h3/a/@href',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'image' => array(
            'name' => '配图',
            'value' => 'a/img/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'context' => array(
          'value' => '//div[@class=\'article\']',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'link',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'link',
            'target' => 'field_source_url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'post_guokr',
      ),
    ),
    'content_type' => 'rss_guokr',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['rss_guokr'] = $feeds_importer;

  return $export;
}
