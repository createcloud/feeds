<?php
/**
 * @file
 * news_yfwhg_net.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function news_yfwhg_net_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function news_yfwhg_net_node_info() {
  $items = array(
    'news_list_yfwhg_net' => array(
      'name' => t('新闻列表 - 云浮市文化馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_yfwhg_net' => array(
      'name' => t('新闻 - 云浮市文化馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
