<?php
/**
 * @file
 * ccloud_culture_ccitimes_com.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_culture_ccitimes_com_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['culture_ccitimes_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-content-absolute_url';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['culture_ccitimes_com-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-content-ccloud_remove_content';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
      'image' => 0,
    ),
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['culture_ccitimes_com-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-content-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => 'htm',
    'indexes' => '3,4,5,6',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['culture_ccitimes_com-content-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-image-absolute_url';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['culture_ccitimes_com-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['culture_ccitimes_com-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-image-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '.html',
    'indexes' => '3,4,5,6',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['culture_ccitimes_com-image-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_ccitimes_com-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'culture_ccitimes_com';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '131',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['culture_ccitimes_com-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_ccitimes_com-blank_source_1-default_value';
  $feeds_tamper->importer = 'culture_list_ccitimes_com';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['culture_list_ccitimes_com-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_ccitimes_com-feed_source-absolute_url';
  $feeds_tamper->importer = 'culture_list_ccitimes_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['culture_list_ccitimes_com-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_ccitimes_com-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'culture_list_ccitimes_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['culture_list_ccitimes_com-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'culture_list_ccitimes_com-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'culture_list_ccitimes_com';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '//',
    'indexes' => '3,4,5',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['culture_list_ccitimes_com-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  return $export;
}
