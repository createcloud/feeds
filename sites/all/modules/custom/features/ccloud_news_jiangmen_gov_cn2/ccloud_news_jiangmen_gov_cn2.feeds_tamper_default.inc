<?php
/**
 * @file
 * ccloud_news_jiangmen_gov_cn2.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_jiangmen_gov_cn2_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_jiangmen_gov_cn2-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_jiangmen_gov_cn2';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_jiangmen_gov_cn2-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_jiangmen_gov_cn2-content-absolute_url';
  $feeds_tamper->importer = 'news_jiangmen_gov_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_jiangmen_gov_cn2-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_jiangmen_gov_cn2-content-find_replace_regex';
  $feeds_tamper->importer = 'news_jiangmen_gov_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/<\\/?(span|font|em|strong)[^>]{0,}>/i',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_jiangmen_gov_cn2-content-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_jiangmen_gov_cn2-content-find_replace_regex_2';
  $feeds_tamper->importer = 'news_jiangmen_gov_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(style|class|face|lang|id|color)="[^"]+"/i',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_jiangmen_gov_cn2-content-find_replace_regex_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_jiangmen_gov_cn2-content-find_replace_regex_3';
  $feeds_tamper->importer = 'news_jiangmen_gov_cn2';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(<p>)(\\s|\\n|\\r|\\&nbsp\\;|　|\\xc2\\xa0)*/m',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_jiangmen_gov_cn2-content-find_replace_regex_3'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_jiangmen_gov_cn2-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_jiangmen_gov_cn2';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_jiangmen_gov_cn2-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_jiangmen_gov_cn2-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_jiangmen_gov_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_jiangmen_gov_cn2-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_jiangmen_gov_cn2-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_jiangmen_gov_cn2';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_jiangmen_gov_cn2-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_jiangmen_gov_cn2-image-absolute_url';
  $feeds_tamper->importer = 'news_list_jiangmen_gov_cn2';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_jiangmen_gov_cn2-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_jiangmen_gov_cn2-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_jiangmen_gov_cn2';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_jiangmen_gov_cn2-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_jiangmen_gov_cn2-title-required';
  $feeds_tamper->importer = 'news_list_jiangmen_gov_cn2';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Required field';
  $export['news_list_jiangmen_gov_cn2-title-required'] = $feeds_tamper;

  return $export;
}
