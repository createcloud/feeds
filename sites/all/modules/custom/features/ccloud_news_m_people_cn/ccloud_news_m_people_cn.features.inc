<?php
/**
 * @file
 * ccloud_news_m_people_cn.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_m_people_cn_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_m_people_cn_node_info() {
  $items = array(
    'news_list_m_people_cn' => array(
      'name' => t('新闻列表 - 手机人民网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'news_m_people_cn' => array(
      'name' => t('新闻 - 手机人民网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
