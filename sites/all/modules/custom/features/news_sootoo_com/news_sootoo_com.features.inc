<?php
/**
 * @file
 * news_sootoo_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function news_sootoo_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function news_sootoo_com_node_info() {
  $items = array(
    'news_list_sootoo_com' => array(
      'name' => t('新闻列表 - 速途网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_sootoo_com' => array(
      'name' => t('新闻 - 速途网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
