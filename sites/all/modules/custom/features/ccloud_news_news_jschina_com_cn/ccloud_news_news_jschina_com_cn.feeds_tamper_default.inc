<?php
/**
 * @file
 * ccloud_news_news_jschina_com_cn.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_news_jschina_com_cn_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_news_jschina_com_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_news_jschina_com_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_news_jschina_com_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_jschina_com_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_news_jschina_com_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_news_jschina_com_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_jschina_com_cn-content-ccloud_remove_content';
  $feeds_tamper->importer = 'news_news_jschina_com_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'author' => 0,
      'publication_date' => 0,
      'source' => 0,
      'next_page_url' => 0,
      'content_to_remove' => 1,
      'image' => 0,
      'remove_comment' => 1,
    ),
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['news_news_jschina_com_cn-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_news_jschina_com_cn-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_news_jschina_com_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_news_jschina_com_cn-image-ccloud_restrict_image'] = $feeds_tamper;

  return $export;
}
