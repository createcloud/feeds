<?php
/**
 * @file
 * ccloud_video_zslib_lecture.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_video_zslib_lecture_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '中山讲堂图文',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440035646,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '3e99ba32-81ed-414e-926a-53818552c970',
  'field_feed_info_feed_type' => array(),
  'field_feed_info_category' => array(),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://lecture.zslib.com.cn/zsjt/zsjttw/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'video_tw_list_zslib_lecture',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-20 09:54:06 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '中山讲堂',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440035150,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '6e8f8652-a6f4-4951-ae0a-2dd66c11465c',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '视频',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '讲座视频',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://lecture.zslib.com.cn/zsjt/zsjtsp/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '广东省立中山图书馆',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'video_list_zslib_lecture',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-20 09:45:50 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '粤海听涛',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440035235,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '7a833712-5168-479d-9b20-4a7ca5826c2b',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '视频',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '讲座视频',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://lecture.zslib.com.cn/yhtt/yhttsp/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '广东省立中山图书馆',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'video_list_zslib_lecture',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-20 09:47:15 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '文化论坛图文',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440035609,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '8f5821fe-68da-41b3-9ac0-0e6b6680c079',
  'field_feed_info_feed_type' => array(),
  'field_feed_info_category' => array(),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://lecture.zslib.com.cn/whlt/whlttw/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'video_tw_list_zslib_lecture',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-20 09:53:29 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '文化论坛',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440035288,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '9ceff242-1ae4-436c-91ea-56d259026979',
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '视频',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '讲座视频',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://lecture.zslib.com.cn/whlt/whltsp/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '广东省立中山图书馆',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'video_list_zslib_lecture',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-20 09:48:08 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  $nodes[] = array(
  'title' => '粤海听涛图文',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'ds_switch' => '',
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1440035577,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'f8569ab8-6447-4187-a257-957573f12419',
  'field_feed_info_feed_type' => array(),
  'field_feed_info_category' => array(),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://lecture.zslib.com.cn/yhtt/yhtttw/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'video_tw_list_zslib_lecture',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-08-20 09:52:57 +0800',
  'user_uuid' => '843fabef-edbd-46f6-a78e-3ded5ded580a',
);
  return $nodes;
}
