<?php
/**
 * @file
 * ccloud_video_zslib_lecture.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_video_zslib_lecture_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_video_zslib_lecture_node_info() {
  $items = array(
    'video_list_zslib_lecture' => array(
      'name' => t('视频列表 - 中山图书馆讲座'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'video_tw_list_zslib_lecture' => array(
      'name' => t('视频图文列表 - 中山图书馆讲座'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'video_tw_zslib_lecture' => array(
      'name' => t('视频图文 - 中山图书馆讲座'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'video_zslib_lecture' => array(
      'name' => t('视频 - 中山图书馆讲座'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
