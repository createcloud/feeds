<?php
/**
 * @file
 * ccloud_video_zslib_lecture.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_video_zslib_lecture_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_list_zslib_lecture';
  $feeds_importer->config = array(
    'name' => '视频列表 - 中山图书馆讲座',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => '.',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '2',
          ),
          'topic' => array(
            'name' => 'Topic',
            'value' => '.',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'context' => array(
          'value' => '//table[@width=\'550px\']//tr[@height=\'100%\']//p/a',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'format' => 'filtered_html',
          ),
          5 => array(
            'source' => 'Blank source 2',
            'target' => 'field_topic',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Blank source 3',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'video_zslib_lecture',
      ),
    ),
    'content_type' => 'video_list_zslib_lecture',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_list_zslib_lecture'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_tw_list_zslib_lecture';
  $feeds_importer->config = array(
    'name' => '视频图文列表 - 中山图书馆讲座',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => '.',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '2',
          ),
        ),
        'context' => array(
          'value' => '//p/a',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'format' => 'filtered_html',
          ),
          5 => array(
            'source' => 'Blank source 2',
            'target' => 'field_topic',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'video_tw_zslib_lecture',
      ),
    ),
    'content_type' => 'video_tw_list_zslib_lecture',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_tw_list_zslib_lecture'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_tw_zslib_lecture';
  $feeds_importer->config = array(
    'name' => '视频图文 - 中山图书馆讲座',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//table[@style=" width:700px;"]//td',
        ),
        'sources' => array(
          'content' => array(
            'name' => 'Content',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '1',
          ),
          'title' => array(
            'name' => 'Title',
            'value' => '//table//td[contains(@style, \'height:40px;\')=\'true\']/p',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'content',
            'target' => 'field_content',
            'format' => 'full_html',
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Blank source 2',
            'target' => 'field_topic',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'parent:nid',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'video_tw_zslib_lecture',
      ),
    ),
    'content_type' => 'video_tw_zslib_lecture',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_tw_zslib_lecture'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_zslib_lecture';
  $feeds_importer->config = array(
    'name' => '视频 - 中山图书馆讲座',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//video',
        ),
        'sources' => array(
          'video_url' => array(
            'name' => 'Video URL',
            'value' => 'source/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => '.',
            'raw' => 1,
            'debug' => 0,
            'weight' => '2',
          ),
          'title' => array(
            'name' => 'Title',
            'value' => '//table//td[contains(@style, \'height:40px;\')=\'true\']/p',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'video_url',
            'target' => 'field_video_url',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'parent:field_source_url',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Blank source 2',
            'target' => 'field_topic',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'parent:nid',
            'target' => 'Temporary target 2',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'video_zslib_lecture',
      ),
    ),
    'content_type' => 'video_zslib_lecture',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_zslib_lecture'] = $feeds_importer;

  return $export;
}
