<?php
/**
 * @file
 * news_163.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function news_163_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_163';
  $feeds_importer->config = array(
    'name' => '新闻 - 网易',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'image' => array(
            'name' => '图片',
            'value' => 'div[@id="endText"]//img[1]/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
          'source' => array(
            'name' => '来源',
            'value' => 'div[1]/div[1]/a',
            'raw' => 0,
            'debug' => 0,
            'weight' => '4',
          ),
          'date' => array(
            'name' => '时间',
            'value' => 'div[1]/div[1]',
            'raw' => 0,
            'debug' => 0,
            'weight' => '5',
          ),
          'author' => array(
            'name' => '作者',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '6',
          ),
          'content' => array(
            'name' => '正文',
            'value' => 'div[@id="endText"]',
            'raw' => 1,
            'debug' => 0,
            'weight' => '7',
          ),
          'video' => array(
            'name' => 'Video',
            'value' => 'div[@id="endText"]//div[@class=\'video-wrapper\']',
            'raw' => 1,
            'debug' => 0,
            'weight' => '8',
          ),
        ),
        'context' => array(
          'value' => '//div[@id=\'epContentLeft\']',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'video',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'news_163',
      ),
    ),
    'content_type' => 'news_163',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_163'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_list_163';
  $feeds_importer->config = array(
    'name' => 'RSS - 网易新闻',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
        'one_time_source' => FALSE,
        'user_agent' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => '标题',
            'value' => 'h2/a',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'link' => array(
            'name' => '链接',
            'value' => 'h2/a/@href',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
        ),
        'context' => array(
          'value' => '//div[@class="item-top"]',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'link',
            'target' => 'field_source_url',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'link',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'news_163',
      ),
    ),
    'content_type' => 'news_list_163',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_list_163'] = $feeds_importer;

  return $export;
}
