<?php
/**
 * @file
 * news_163.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function news_163_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_163-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_163';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_163-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_163-content-implode';
  $feeds_tamper->importer = 'news_163';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'implode';
  $feeds_tamper->settings = array(
    'glue' => '%n',
    'real_glue' => '
',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Implode';
  $export['news_163-content-implode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_163-content-php';
  $feeds_tamper->importer = 'news_163';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$field_array = explode(\'<***>\', $field);
$content = $field_array[0];
$video = $field_array[1];
return str_replace($video, \'\', $content);',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Execute php code';
  $export['news_163-content-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_163-content-rewrite';
  $feeds_tamper->importer = 'news_163';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[content]<***>[video]',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Rewrite';
  $export['news_163-content-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_163-content-strip_tags';
  $feeds_tamper->importer = 'news_163';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<p><a><img><em><strong><h3><h4><h5><br><style>',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Strip tags';
  $export['news_163-content-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_163-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_163';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_163-blank_source_1-default_value'] = $feeds_tamper;

  return $export;
}
