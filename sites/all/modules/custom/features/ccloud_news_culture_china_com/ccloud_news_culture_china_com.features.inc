<?php
/**
 * @file
 * ccloud_news_culture_china_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_culture_china_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_culture_china_com_node_info() {
  $items = array(
    'news_culture_china_com' => array(
      'name' => t('新闻 - 中华网文化'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'news_list_culture_china_com' => array(
      'name' => t('新闻列表 - 中华网文化'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
