<?php
/**
 * @file
 * ccloud_video_whg.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_video_whg_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_video_whg_node_info() {
  $items = array(
    'video_list_whg' => array(
      'name' => t('视频列表 - 文化馆稀有剧种'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'video_whg' => array(
      'name' => t('视频 - 文化馆稀有剧种'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
