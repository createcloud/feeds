<?php
/**
 * @file
 * ccloud_video_whg.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_video_whg_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'video_list_whg';
  $feeds_importer->config = array(
    'name' => '视频列表 - 文化馆稀有剧种',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExJsonPath',
      'config' => array(
        'context' => array(
          'value' => '$.*',
        ),
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => 'title',
            'debug' => 0,
            'weight' => '1',
          ),
          'thumbnail' => array(
            'name' => 'Thumbnail',
            'value' => 'video_thumbnail',
            'debug' => 0,
            'weight' => '2',
          ),
          'video' => array(
            'name' => 'Video',
            'value' => 'video_src',
            'debug' => 0,
            'weight' => '3',
          ),
          'created' => array(
            'name' => 'Created',
            'value' => 'created',
            'debug' => 0,
            'weight' => '4',
          ),
          'nid' => array(
            'name' => 'Nid',
            'value' => 'nid',
            'debug' => 0,
            'weight' => '5',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 1,
        'source_encoding' => array(
          0 => 'auto',
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 0,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_source_url',
            'format' => 'filtered_html',
          ),
          5 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'video',
            'target' => 'field_video_url',
            'format' => 'plain_text',
          ),
          7 => array(
            'source' => 'thumbnail',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'video_whg',
      ),
    ),
    'content_type' => 'video_list_whg',
    'update' => 0,
    'import_period' => '604800',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['video_list_whg'] = $feeds_importer;

  return $export;
}
