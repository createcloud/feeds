<?php
/**
 * @file
 * ccloud_paper_news_e_chengdu.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_paper_news_e_chengdu_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_e_chengdu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-content-absolute_url';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_e_chengdu-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-content-ccloud_remove_content';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_content';
  $feeds_tamper->settings = array(
    'source' => array(
      'Blank source 1' => 0,
      'content' => 0,
      'image' => 0,
      'author' => 0,
      'source' => 0,
      'publication_date' => 0,
      'content_to_remove' => 1,
    ),
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Remove content of sources';
  $export['paper_news_e_chengdu-content-ccloud_remove_content'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-content-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '../../../',
    'indexes' => '3,4,5,6,7,8,9',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_e_chengdu-content-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-content-html_entity_encode';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = '';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'HTML entity encode';
  $export['paper_news_e_chengdu-content-html_entity_encode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-image-absolute_url';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_e_chengdu-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_e_chengdu-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-image-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
    'included' => '../../../',
    'indexes' => '3,4,5,6,7,8,9',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_e_chengdu-image-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['paper_news_e_chengdu-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-publication_date-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(\\d{4}年\\d{1,2}月\\d{1,2}日)([\\s|\\S]+)/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_e_chengdu-publication_date-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_e_chengdu-source-find_replace_regex';
  $feeds_tamper->importer = 'paper_news_e_chengdu';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(\\d{4}年\\d{1,2}月\\d{1,2}日)(.*?)/',
    'replace' => '$2',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['paper_news_e_chengdu-source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_e_chengdu-blank_source_1-default_value';
  $feeds_tamper->importer = 'paper_news_list_1_e_chengdu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['paper_news_list_1_e_chengdu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_e_chengdu-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_1_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_1_e_chengdu-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_e_chengdu-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_list_1_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_list_1_e_chengdu-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_e_chengdu-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_list_1_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.htm/',
    'indexes' => '6',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_1_e_chengdu-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_1_e_chengdu-title-keyword_filter';
  $feeds_tamper->importer = 'paper_news_list_1_e_chengdu';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => '广告',
    'word_boundaries' => 1,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 1,
    'word_list' => array(
      0 => '/\\b广告\\b/ui',
    ),
    'regex' => TRUE,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['paper_news_list_1_e_chengdu-title-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_e_chengdu-feed_source-absolute_url';
  $feeds_tamper->importer = 'paper_news_list_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['paper_news_list_e_chengdu-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_e_chengdu-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'paper_news_list_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['paper_news_list_e_chengdu-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_e_chengdu-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'paper_news_list_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.htm/./',
    'indexes' => '6,7',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_e_chengdu-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'paper_news_list_e_chengdu-feed_source-ccloud_remove_url_segment_2';
  $feeds_tamper->importer = 'paper_news_list_e_chengdu';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => '.htm/',
    'indexes' => '6',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['paper_news_list_e_chengdu-feed_source-ccloud_remove_url_segment_2'] = $feeds_tamper;

  return $export;
}
