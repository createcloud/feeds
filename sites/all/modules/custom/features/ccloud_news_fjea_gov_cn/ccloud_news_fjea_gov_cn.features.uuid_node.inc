<?php
/**
 * @file
 * ccloud_news_fjea_gov_cn.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_news_fjea_gov_cn_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '新闻列表 - 福建省地震局 - 政策法规',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1445966245,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '735a1107-1720-4806-af1c-a7b36a384abf',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '新闻',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '福建省地震局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.fjea.gov.cn/NewsList.aspx?ClassID=6',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_fjea_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_area' => array(),
  'field_feed_info_gdnews_type' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-10-27 17:17:25 +0000',
  'user_uuid' => 'c70ba0ea-403d-4a48-bbcb-793d9302d8ab',
);
  $nodes[] = array(
  'title' => '新闻列表 - 福建省地震局 - 地震历史文献',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1445966447,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '8542a076-19c6-4fba-957d-aed1783a5c78',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '新闻',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '福建省地震局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.fjea.gov.cn/NewsList.aspx?ClassID=18',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_fjea_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_area' => array(),
  'field_feed_info_gdnews_type' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-10-27 17:20:47 +0000',
  'user_uuid' => 'c70ba0ea-403d-4a48-bbcb-793d9302d8ab',
);
  $nodes[] = array(
  'title' => '新闻列表 - 福建省地震局 - 学术科技动态',
  'log' => '',
  'status' => 0,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1446002580,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'e8b527b8-e7e0-4d4c-ae4d-f33da5a399ae',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '新闻',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '福建省地震局',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed' => array(),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.fjea.gov.cn/NewsList.aspx?ClassID=27',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_fjea_gov_cn',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_np_list_type' => array(),
  'field_feed_info_np_url_pattern' => array(),
  'field_feed_info_gdnews_area' => array(),
  'field_feed_info_gdnews_type' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2015-10-28 03:23:00 +0000',
  'user_uuid' => 'c70ba0ea-403d-4a48-bbcb-793d9302d8ab',
);
  return $nodes;
}
