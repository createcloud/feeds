<?php
/**
 * @file
 * ccloud_feeds_common.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ccloud_feeds_common_default_rules_configuration() {
  $items = array();
  $items['ccloud_feed_item_operation_ccloud_activate_feed'] = entity_import('rules_config', '{ "ccloud_feed_item_operation_ccloud_activate_feed" : {
      "LABEL" : "CCloud: Activate feed",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "createcloud_feeds" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "feed_info" : "feed_info" } }
                }
              }
            ],
            "DO" : [ { "ccloud_feeds_feed_activate" : { "node" : [ "node" ] } } ],
            "LABEL" : "Activate"
          }
        }
      ]
    }
  }');
  $items['ccloud_feed_item_operation_ccloud_deactivate_feed'] = entity_import('rules_config', '{ "ccloud_feed_item_operation_ccloud_deactivate_feed" : {
      "LABEL" : "CCloud: Deactivate feed",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "createcloud_feeds" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "feed_info" : "feed_info" } }
                }
              }
            ],
            "DO" : [ { "ccloud_feeds_feed_deactivate" : { "node" : [ "node" ] } } ],
            "LABEL" : "Deactivate"
          }
        }
      ]
    }
  }');
  return $items;
}
