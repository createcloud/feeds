<?php
/**
 * @file
 * ccloud_feeds_common.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function ccloud_feeds_common_image_default_styles() {
  $styles = array();

  // Exported image style: ccloud_feeds_image_style.
  $styles['ccloud_feeds_image_style'] = array(
    'name' => 'ccloud_feeds_image_style',
    'label' => 'Feeds image style',
    'effects' => array(
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 500,
          'height' => 500,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function ccloud_feeds_common_node_info() {
  $items = array(
    'feed_info' => array(
      'name' => t('Feed info'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
