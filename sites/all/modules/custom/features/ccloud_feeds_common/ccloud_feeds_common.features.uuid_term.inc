<?php
/**
 * @file
 * ccloud_feeds_common.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function ccloud_feeds_common_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '问答',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '75d2c3e8-9193-4ebb-a009-b03d59d733cf',
    'vocabulary_machine_name' => 'feed_type',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'question',
          'format' => NULL,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => '资讯',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7d205aa9-ac69-4f63-bebc-4235c4763c69',
    'vocabulary_machine_name' => 'feed_type',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'news',
          'format' => NULL,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => '图书',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a898c503-65a1-452a-8b78-a87e9679e7bb',
    'vocabulary_machine_name' => 'feed_type',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'book',
          'format' => NULL,
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => '报纸',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'f8939f09-923b-44dc-8693-062893ac8a5c',
    'vocabulary_machine_name' => 'feed_type',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'news-paper',
          'format' => NULL,
        ),
      ),
    ),
  );
  return $terms;
}
