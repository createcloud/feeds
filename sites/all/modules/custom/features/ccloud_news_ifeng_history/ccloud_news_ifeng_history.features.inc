<?php
/**
 * @file
 * ccloud_news_ifeng_history.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_ifeng_history_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_ifeng_history_node_info() {
  $items = array(
    'news_ifeng_history' => array(
      'name' => t('新闻 - 凤凰历史'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'news_list_ifeng_history' => array(
      'name' => t('新闻列表 - 凤凰历史'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
