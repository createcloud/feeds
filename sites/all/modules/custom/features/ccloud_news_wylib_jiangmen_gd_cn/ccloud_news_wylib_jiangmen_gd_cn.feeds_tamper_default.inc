<?php
/**
 * @file
 * ccloud_news_wylib_jiangmen_gd_cn.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_wylib_jiangmen_gd_cn_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_wylib_jiangmen_gd_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_list_wylib_jiangmen_gd_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_wylib_jiangmen_gd_cn-feed_source-absolute_url';
  $feeds_tamper->importer = 'news_list_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_wylib_jiangmen_gd_cn-feed_source-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_wylib_jiangmen_gd_cn-feed_source-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_wylib_jiangmen_gd_cn-feed_source-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_wylib_jiangmen_gd_cn-feed_source-ccloud_remove_url_segment';
  $feeds_tamper->importer = 'news_list_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'feed_source';
  $feeds_tamper->plugin_id = 'ccloud_remove_url_segment';
  $feeds_tamper->settings = array(
    'tag' => 'a',
    'attribute' => 'href',
    'included' => 'html',
    'indexes' => '3,4,5,6',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Remove segments of url attribute';
  $export['news_list_wylib_jiangmen_gd_cn-feed_source-ccloud_remove_url_segment'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['news_wylib_jiangmen_gd_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-blank_source_2-php';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$img_match = array();
$src_match = array();
$img_url = \'\';
if (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  if (preg_match(\'/(src)="([^"]*)"/i\', $img_match[0], $src_match)) {
    $img_url = $src_match[2];
  }
}
return $img_url;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['news_wylib_jiangmen_gd_cn-blank_source_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-blank_source_2-rewrite';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[content]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['news_wylib_jiangmen_gd_cn-blank_source_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-content-absolute_url';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_wylib_jiangmen_gd_cn-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-content-absolute_url_2';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_wylib_jiangmen_gd_cn-content-absolute_url_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-content-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'iframe',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_wylib_jiangmen_gd_cn-content-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-content-find_replace';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'class="text1"',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Find replace';
  $export['news_wylib_jiangmen_gd_cn-content-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-content-php';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$response = drupal_http_request($field);
$field = \'\';
if ($response->code == \'200\' && $response->status_message == \'OK\') {
  $data = !empty($response->data) ? $response->data : \'\';
  if (preg_match(\'/<body >(.+)<\\/body>/s\', $data, $tag_match)) {
    if (!empty($tag_match[1])) {
      $field = $tag_match[1];
    }
  }
}
return $field;',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';
  $export['news_wylib_jiangmen_gd_cn-content-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_wylib_jiangmen_gd_cn-publication_date-find_replace';
  $feeds_tamper->importer = 'news_wylib_jiangmen_gd_cn';
  $feeds_tamper->source = 'publication_date';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => '发表时间： ',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace';
  $export['news_wylib_jiangmen_gd_cn-publication_date-find_replace'] = $feeds_tamper;

  return $export;
}
