<?php
/**
 * @file
 * ccloud_news_gdwhg.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_gdwhg_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_gdwhg_node_info() {
  $items = array(
    'item_gdwhg' => array(
      'name' => t('ITEM - 广东文化馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rss_gdwhg' => array(
      'name' => t('RSS - 广东文化馆'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
