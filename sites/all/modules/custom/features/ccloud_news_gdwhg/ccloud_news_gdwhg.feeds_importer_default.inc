<?php
/**
 * @file
 * ccloud_news_gdwhg.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_news_gdwhg_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'item_gdwhg';
  $feeds_importer->config = array(
    'name' => 'ITEM - 广东文化馆',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//table[@class=\'qy-z2\']',
        ),
        'sources' => array(
          'title' => array(
            'name' => '标题',
            'value' => '//td[@class=\'qy-y5\']',
            'raw' => 0,
            'debug' => 0,
            'weight' => '0',
          ),
          'author' => array(
            'name' => '作者',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'date' => array(
            'name' => '时间',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'content' => array(
            'name' => '正文',
            'value' => '//td[@class=\'qy-y6\']',
            'raw' => 1,
            'debug' => 0,
            'weight' => '3',
          ),
          'source' => array(
            'name' => '来源',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '4',
          ),
          'image' => array(
            'name' => '图片',
            'value' => '//td[@class=\'qy-y6\']//img[1]/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '5',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'content',
            'target' => 'field_content',
            'format' => 'full_html',
          ),
          6 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'parent:field_image',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'parent:feed_source',
            'target' => 'field_source_url',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'parent:taxonomy:category',
            'target' => 'field_category',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          10 => array(
            'source' => 'parent:taxonomy:channel',
            'target' => 'field_channel',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          11 => array(
            'source' => 'parent:taxonomy:feed_type',
            'target' => 'field_feed_type',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          12 => array(
            'source' => 'parent:field_er_feed',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'news',
      ),
    ),
    'content_type' => 'item_gdwhg',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['item_gdwhg'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'rss_gdwhg';
  $feeds_importer->config = array(
    'name' => 'RSS - 广东文化馆',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 0,
        'user_agent' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//*[@id="FocusWrap"]/div[3]/div[1]/ul/li',
        ),
        'sources' => array(
          'title' => array(
            'name' => '标题',
            'value' => 'a',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'link' => array(
            'name' => '链接',
            'value' => 'a/@href',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'cover_image' => array(
            'name' => '封面图',
            'value' => '',
            'raw' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'link',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'cover_image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'parent:taxonomy:category',
            'target' => 'field_category',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          6 => array(
            'source' => 'parent:taxonomy:channel',
            'target' => 'field_channel',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          7 => array(
            'source' => 'parent:taxonomy:feed_type',
            'target' => 'field_feed_type',
            'term_search' => '1',
            'autocreate' => 0,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'item_gdwhg',
      ),
    ),
    'content_type' => 'rss_gdwhg',
    'update' => 0,
    'import_period' => '3600',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['rss_gdwhg'] = $feeds_importer;

  return $export;
}
