<?php
/**
 * @file
 * ccloud_news_gdwhg.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_news_gdwhg_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'item_gdwhg-content-absolute_url';
  $feeds_tamper->importer = 'item_gdwhg';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['item_gdwhg-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'item_gdwhg-content-strip_tags';
  $feeds_tamper->importer = 'item_gdwhg';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<p><a><img><em><strong><h3><h4><h5><br>',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Strip tags';
  $export['item_gdwhg-content-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'item_gdwhg-image-php';
  $feeds_tamper->importer = 'item_gdwhg';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'foreach($field as &$img) {
  $img = \'http://www.gdsqyg.com\'. $img;
}

return $field;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['item_gdwhg-image-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_gdwhg-link-php';
  $feeds_tamper->importer = 'rss_gdwhg';
  $feeds_tamper->source = 'link';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return \'http://www.gdsqyg.com\'. str_replace(\'http://www.gdsqyg.com\', \'\', $field);',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Add host';
  $export['rss_gdwhg-link-php'] = $feeds_tamper;

  return $export;
}
