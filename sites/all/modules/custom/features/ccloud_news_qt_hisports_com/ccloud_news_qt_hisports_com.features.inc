<?php
/**
 * @file
 * ccloud_news_qt_hisports_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_qt_hisports_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_qt_hisports_com_node_info() {
  $items = array(
    'sports_list_qt_hisports_com' => array(
      'name' => t('体育列表 - 五星体育'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sports_qt_hisports_com' => array(
      'name' => t('体育 - 五星体育'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
