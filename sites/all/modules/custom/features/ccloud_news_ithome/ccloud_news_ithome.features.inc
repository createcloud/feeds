<?php
/**
 * @file
 * ccloud_news_ithome.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_news_ithome_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_news_ithome_node_info() {
  $items = array(
    'news_ithome' => array(
      'name' => t('新闻 - IT之家'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rss_ithome' => array(
      'name' => t('RSS - IT之家'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
