<?php
/**
 * @file
 * ccloud_news_ithome.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_news_ithome_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_ithome';
  $feeds_importer->config = array(
    'name' => '新闻 - IT之家',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'context' => array(
          'value' => '//div[@id=\'wrapper\']/div[1]',
        ),
        'sources' => array(
          'author' => array(
            'name' => '作者',
            'value' => '//*[@id="author_baidu"]/strong',
            'raw' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'date' => array(
            'name' => '时间',
            'value' => '//*[@id="pubtime_baidu"]',
            'raw' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'content' => array(
            'name' => '正文',
            'value' => 'div[@id=\'paragraph\']',
            'raw' => 1,
            'debug' => 0,
            'weight' => '4',
          ),
          'source' => array(
            'name' => '来源',
            'value' => '//*[@id="source_baidu"]/a',
            'raw' => 0,
            'debug' => 0,
            'weight' => '5',
          ),
          'image' => array(
            'name' => '图片',
            'value' => 'div[@id=\'paragraph\']//img[1]/@src',
            'raw' => 0,
            'debug' => 0,
            'weight' => '6',
          ),
        ),
        'display_errors' => 1,
        'debug_mode' => 1,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'use_tidy' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'date',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'news_ithome',
      ),
    ),
    'content_type' => 'news_ithome',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_ithome'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'rss_ithome';
  $feeds_importer->config = array(
    'name' => 'RSS - IT之家',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CreateCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
        'one_time_source' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsSyndicationParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'parent:nid',
            'target' => 'field_er_feed:etid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'url',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'url',
            'target' => 'field_source_url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'timestamp',
            'target' => 'created',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'timestamp',
            'target' => 'field_publication_date',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'news_ithome',
      ),
    ),
    'content_type' => 'rss_ithome',
    'update' => 0,
    'import_period' => '21600',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['rss_ithome'] = $feeds_importer;

  return $export;
}
