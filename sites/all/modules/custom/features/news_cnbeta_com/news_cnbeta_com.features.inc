<?php
/**
 * @file
 * news_cnbeta_com.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function news_cnbeta_com_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function news_cnbeta_com_node_info() {
  $items = array(
    'news_cnbeta_com' => array(
      'name' => t('新闻 - cnBeta'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_list_cnbeta_com' => array(
      'name' => t('新闻列表 - cnBeta'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
