<?php
/**
 * @file
 * ccloud_qna_zhihu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_qna_zhihu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_qna_zhihu_node_info() {
  $items = array(
    'answer_zhihu' => array(
      'name' => t('回答 - 知乎'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'question_list_zhihu' => array(
      'name' => t('问题列表 - 知乎'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'question_zhihu' => array(
      'name' => t('问题 - 知乎'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
