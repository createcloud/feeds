<?php
/**
 * @file
 * ccloud_qna_zhihu.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_qna_zhihu_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-blank_source_1-default_value';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['answer_zhihu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-blank_source_2-php';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$img_match = array();
$src_match = array();
$img_url = \'\';
if (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  if (preg_match(\'/(src)="([^"]*)"/i\', $img_match[0], $src_match))
    $img_url = ($src_match[2]);
}

return $img_url;',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Execute php code';
  $export['answer_zhihu-blank_source_2-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-blank_source_2-rewrite';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[xpathparser:2][xpathparser:0]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['answer_zhihu-blank_source_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_0-absolute_url';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['answer_zhihu-xpathparser_0-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_0-php';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$field_upd = $field;

$img_match = array();
$src_match = array();
$data_original_match = array();
$data_actualsrc_match = array();

$img = \'\';
$src_url = \'\';
$data_original_url = \'\';
$data_actualsrc_url = \'\';

$src_exist = false;
$data_original_exist = false;
$data_actualsrc_exist = true;

while (preg_match("/<img[^>]+>/i", $field, $img_match)) {
  $img = $img_match[0];
//  dpm($img, \'img\');

  if (preg_match(\'/(src)="([^"]*)"/i\', $img, $src_match)) {
    $src_url = $src_match[2];
//    dpm($src_url, \'src_url\');
//    $src_exist = drupal_http_request($src_url);
//    dpm($src_exist, \'src exists\');
  }

  if (preg_match(\'/(data-original)="([^"]*)"/i\', $img, $data_original_match)) {
    $data_original_url = $data_original_match[2];
//    dpm($data_original_url, \'data_original_url\');
//    $data_original_exist = drupal_http_request($data_original_url);
//    dpm($data_original_exist, \'data_original exists\');
  }

  if (preg_match(\'/(data-actualsrc)="([^"]*)"/i\', $img, $data_actualsrc_match)) {
    $data_actualsrc_url = $data_actualsrc_match[2];
//    dpm($data_actualsrc_url, \'data_actualsrc_url\');
//    $data_actualsrc_exist = drupal_http_request($data_actualsrc_url);
//    dpm($data_actualsrc_exist, \'data_actualsrc exists\');
  }

  if (!$src_exist) {
    if ($data_original_exist) {
      $img = str_replace($src_url, $data_original_url, $img);
      //    $img = "<img src=\\"$data_original_url\\">";
    } elseif ($data_actualsrc_exist) {
      $img = str_replace($src_url, $data_actualsrc_url, $img);
      //    $img = "<img src=\\"$data_actualsrc_url\\">";
    }

      $field_upd = str_replace($img_match[0], $img, $field_upd);
      $field = str_replace($img_match[0], \'\', $field);
  }
//  dpm($field_upd, \'field upd\');
}

return $field_upd;',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'Execute php code';
  $export['answer_zhihu-xpathparser_0-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_0-strip_tags';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<br>',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Strip tags';
  $export['answer_zhihu-xpathparser_0-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_0-trim';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Trim';
  $export['answer_zhihu-xpathparser_0-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_1-trim';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['answer_zhihu-xpathparser_1-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_2-absolute_url';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['answer_zhihu-xpathparser_2-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_2-strip_tags';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<br>',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Strip tags';
  $export['answer_zhihu-xpathparser_2-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'answer_zhihu-xpathparser_2-trim';
  $feeds_tamper->importer = 'answer_zhihu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Trim';
  $export['answer_zhihu-xpathparser_2-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_list_zhihu-blank_source_1-default_value';
  $feeds_tamper->importer = 'question_list_zhihu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['question_list_zhihu-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_list_zhihu-xpathparser_0-rewrite';
  $feeds_tamper->importer = 'question_list_zhihu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => 'http://www.zhihu.com[xpathparser:0]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['question_list_zhihu-xpathparser_0-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_list_zhihu-xpathparser_2-rewrite';
  $feeds_tamper->importer = 'question_list_zhihu';
  $feeds_tamper->source = 'xpathparser:2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => 'http://www.zhihu.com[xpathparser:2]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['question_list_zhihu-xpathparser_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_zhihu-blank_source_1-rewrite';
  $feeds_tamper->importer = 'question_zhihu';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '回答',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['question_zhihu-blank_source_1-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_zhihu-blank_source_2-default_value';
  $feeds_tamper->importer = 'question_zhihu';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['question_zhihu-blank_source_2-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_zhihu-xpathparser_0-rewrite';
  $feeds_tamper->importer = 'question_zhihu';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => 'http://www.zhihu.com[xpathparser:0]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['question_zhihu-xpathparser_0-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'question_zhihu-xpathparser_1-rewrite';
  $feeds_tamper->importer = 'question_zhihu';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => 'http://www.zhihu.com[xpathparser:1]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['question_zhihu-xpathparser_1-rewrite'] = $feeds_tamper;

  return $export;
}
